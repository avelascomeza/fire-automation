const EC = protractor.ExpectedConditions;

/** 
 * describes the eso-text control from the grizzly framework 
 * */
class EsoText {

    static getEsoTextField(fieldRef) { return $(`eso-text[field-ref='${fieldRef}'] input`); }

    static getEsoTextFieldValue(fieldRef) { return $(`eso-text[field-ref='${fieldRef}'] input`).getText(); }

    static setEsoTextField(fieldRef, text) { $(`eso-text[field-ref='${fieldRef}'] input`).sendkeys(text); }
}

/** 
 * describes the eso-single-select control from the grizzly framework 
 * */
class EsoSingleSelect {

    static getEsoSingleSelect(fieldRef) { return $(`eso-single-select[field-ref='${fieldRef}']`)}

    static shelfIsDisplayed() { return $("eso-single-select-shelf").isDisplayed(); }

    static getShelfOptions() { return $$("eso-single-select-panel ul li"); }

    static setEsoSingleSelect(option) {

        browser.wait(() => {return $("eso-single-select-shelf").isDisplayed(); })               
        .then(() => {
            this.getShelfOptions().then(options => { options[option].click();  } )
            browser.wait(EC.invisibilityOf($('veil'), 5000));
        })
    }

    static setEsoSingleSelectSearch(option) {

        browser.wait(() => {return $("eso-single-select-shelf").isDisplayed(); })               
        .then(() => {
            $("eso-search-input input").sendKeys(option);
            this.getShelfOptions().then(options => { options[0].click();  } )
            browser.wait(EC.invisibilityOf($('veil'), 5000));
        })
    }
}

class EsoDate {

    static getEsoDate(fieldRef) { return $(`eso-date[field-ref='${fieldRef}']`)}

    static dateShelfIsDisplayed()  { return $("eso-date-panel").isDisplayed(); }

    static waitForDateShelfOpen() { 
        browser.wait(() => {return $("eso-date-panel").isDisplayed();})
        browser.wait(EC.elementToBeClickable($("eso-masked-input"), 5000));
    } 

    static waitForDateShelfClose() { 
        browser.wait(EC.invisibilityOf($('eso-shelf'), 5000));
    } 

    static clickOK() {
        this.waitForDateShelfOpen().then(() => { 
            element(by.buttonText("OK")).click();
        })
        this.waitForDateShelfClose();
    }

    static getFieldLabel() { return $("eso-date-panel shelf-panel div header div h1").getText(); }
    

    static setDateWithText(date) {

        this.waitForDateShelfOpen().then(() => { 
            $("eso-masked-input").sendKeys(date);
        })

     //   browser.wait(() => {return $("eso-date-panel").isDisplayed();})
     //   .then(() =>{
     //       $("eso-masked-input").sendKeys(date);
     //       element(by.buttonText("OK")).click();
     //       browser.wait(EC.invisibilityOf($('eso-date-panel'), 5000));
     //   })
    }


    static setDateWithCalender() {
        this.waitForDateShelfOpen().then(() => { 
            $$("eso-date-picker-panel main div.eso-date-picker-panel-viewport ul li").then(options => {
             

            })

        })
    }


    static clearDate() {
        this.waitForDateShelfOpen().then(() => {
            $("a[ng-click='clearDate()']").click();
        })


    }

    



}


class EsoMultiSelect {

    static getMultiSelect(fieldRef) { return $(`eso-multi-select[field-ref='${fieldRef}']`) }

    static multiShelfIsDisplayed()  { return $("eso-multi-select-shelf").isDisplayed(); }

    static waitForMultiShelfOpen() { 
        browser.wait(() => {return this.multiShelfIsDisplayed(); })
        browser.wait(EC.elementToBeClickable($("eso-search-input input"), 5000));
    } 

    static waitForMultiShelfClose() { 
        browser.wait(EC.invisibilityOf($('eso-shelf'), 5000));
    } 

    static getFieldLabel() { return $("eso-multi-select-shelf shelf-panel div header div h1").getText(); }

    static clickOK() {
        this.waitForMultiShelfOpen().then(() => { 
            element(by.buttonText("OK")).click();
        })
        this.waitForMultiShelfClose();
    }

    static getMultiSelectOptions() { return $$("eso-multi-select-panel ul li"); }

    static getMultiSelectOptionsSize() { return this.getMultiSelectOptions().length; }

    static setEsoMultiSelectOption(option) {

        this.waitForMultiShelfOpen().then(() => { 
            this.getMultiSelectOptions().then(options => { options[option].click();  } )
        })
    }

    static setEsoMultiSelectOptionSearch(option) {

        this.waitForMultiShelfOpen().then(() => { 
            $("eso-search-input input").sendKeys(option);
            this.getShelfOptions().then(options => { options[0].click();  } )
        })
    }



}

class EsoCollectionWithAttachments {

    static clickAdd(fieldRef){ return $(`collection-with-attachments[field-ref='${fieldRef}'] grid-row.add grid-cell button`);  }

    static getItemList() { return $$("collection-with-attachments grid-row"); }

    static getItemCount() { return this.getItemList().length; }

    static getItem(option) {
        var itemList = this.getItemList();

        var item = {
            thumbnail: "",
            info: "",
            editLink: "",
            menuButton: ""
        };

        var obj = itemList[option];

        item.thumbnail = obj.$("grid-cell.thumbnail");
        item.info = obj.$("grid-cell.name");
        item.editLink = obj.$("grid-cell.name div.primary div.edit-action");
        item.menuButton = obj.$("grid-cell.remove.icon-cell");

        return item;
    }



    

}







module.exports = {EsoText, EsoSingleSelect, EsoDate, EsoMultiSelect}
