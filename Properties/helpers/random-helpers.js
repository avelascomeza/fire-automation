/**
 * A list of operations that will help in the construction of Strings.
 */
class RandomHelpers {
    
    /**
     * Creates a random string of the given length.
     */
    static randomString(length) {
        var i, key = "", characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        for (i = 0; i < length; i++) {
            key += characters.substr(Math.floor((Math.random() * characters.length) + 1), 1);
        }

        return key;
    }

    static randomInteger(max) {
       return Math.floor(Math.random() * (max))
    }
    

}

module.exports = RandomHelpers;