
let testData = {

mainProperty: {

            number: "8607",
            prefix: 3,              //Northwest
            streetName: "Dawnridge",
            streetType: 17,          //Circle
            suffix: 3,              //Northwest
            city: "Austin",
            state:  54,              //Texas
            zip:   78757,
            county: "Travis",
            mapped: true
    },

uniqueProperty: {

            number: "8611",
            prefix: 3,              //Northwest
            streetName: "Dawnridge",
            streetType: 3,          //Avenue
            suffix: 3,              //Northwest
            city: "Austin",
            state:  3,              //American Samoa
            zip:   78757,
            county: "Travis",
            mapped: false
    }

};


module.exports = testData;