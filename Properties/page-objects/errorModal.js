/** 
 * Popup modal whenever an error occurs. 
 * */
let errorModal = {

    /**
     * Checks to see if the Error Modal is displayed on the page. 
     * 
     * @returns true if the number of generic-error elements on the page is greater than 0. Otherwise, false.
     */


    //generic-error is a dom element (angular directive)
    //then 'resolves' a promise

    checkDisplayed() {

        //elements(by.css("generic-error"))
        // $("")  gets first element
        //$$     gets all elements
        return $$("generic-error").then(modals => { return (modals.length > 0); });
    },

    /** Get all generic-error elements as an array (so we can test for zero). */
    get element() { return $("generic-error") },

    /** The OK button for this modal. */
    get okButton() { return element(by.buttonText("OK")) }
};

module.exports = errorModal;