const EC = protractor.ExpectedConditions;

let propertySearch = {

      searchBarIsDisplayed() {
        return $("div.search-control").isPresent();
    },

    get searchBar() { return $("div.search-control") },

    get searchBarInput() { return $("div.search-control input") },

    get searchButton() { return $("div.search-control div.search-icon")  },

    setSearch(text) {

        this.searchBarInput.sendKeys(text);

    },

    createProperty() {
        browser.wait(EC.elementToBeClickable($("div.create-property-btn"), 5000));           
        $("div.create-property-btn").click();

    },




    get autocompleteContainer() { return $("div.autocomplete-container")  },

    //maybe div.autocomplete-container div.sidebar-property-autocomplete  in front
    get autocompleteResults() { return $$("property-search-autocomplete div.property-autocomplete ul li")  },

    selectAutoCompleteAddress(addressNumName){

        this.autocompleteResults.then(options => {
            options.find(o => o.$("div.property-name").getText() == addressNumName).click();
          //  option.click();
        })


    },



    get searchResultsContainer() { return $("div.sidebar div.search-results-container")  },

    get searchResults() { return $$("property-search-results div.property-search-results div.search-results ul li")  },

    get numberOfResults() { return $("property-search-results div.property-search-results footer div.results-count")  },

};


module.exports = propertySearch;