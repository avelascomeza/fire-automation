const EsoControls = require("../eso-core-controls/esoControls");
const EC = protractor.ExpectedConditions;



var FieldRefs = {
            //HYDRANTS
            hydrantCollection: "PROPERTYWATER_HYDRANTCOMPLEXCOLLECTIONWITHATTACHMENT",
            hydrantID: "PROPERTYHYDRANT_HYDRANTID",
            hydrantLocationDescription: "PROPERTYHYDRANT_HYDRANTLOCATIONDESCRIPTION",
            hydrantCrossStreet: "PROPERTYHYDRANT_CROSSSTREET",
            hydrantFlow: "PROPERTYHYDRANT_FLOW",

            //OTHER SOURCES
            otherSourcesCollection: "PROPERTYWATER_OTHERWATERSOURCESCOMPLEXCOLLECTIONWITHATTACHMENT",
            waterSourceType: "PROPERTYOTHERWATERSOURCE_WATERSOURCETYPE",
            waterSourceLocation: "PROPERTYOTHERWATERSOURCE_WATERSOURCELOCATION",

};

var Tabs = {
    
    map: "property.map",
    property: "property.property",
    contacts: "property.contacts",
    water: "property.water",
    attachments: "property.attachment",

};

var JumpTabs = {
    
    hydrants: "Hydrants",
    otherSources: "Other Sources"
};

let waterTab = {

    isDisplayed() {
        return $("main.eso-app-main.has-tab-nav").isPresent();
    },

    goToTab(tab) {
        $(`section.eso-app-tab-nav nav ul li[ui-sref='property.${tab}']`).click();

    },

    goToJumpTab(jumpTab) {


    },


    //HYDRANTS

    get hydrantAddButton(){
        return $("collection-with-attachments[field-ref='PROPERTY_SPECIALHAZARDSCOMPLEXCOLLECTIONWITHATTACHMENT'] grid-row.add grid-cell button");
    }, 

    get hydrantDoneButton(){
        this.waitForHydrantShelfOpen();   //Make sure shelf is open to click button
        return element(by.buttonText("Done"));
    }, 

    get hydrantCancelButton(){
        this.waitForHydrantShelfOpen();    //Make sure shelf is open to click button
        return element(by.buttonText("Cancel"));
    }, 

    waitForHydrantShelfOpen() { 
        browser.wait(() => {return $("properties-shelf-panel").isDisplayed();})
        browser.wait(EC.elementToBeClickable($("image-field div div div.shelf-click-indicator"), 5000));
    },

    waitForHydrantShelfClose() { 
        browser.wait(EC.invisibilityOf($('eso-shelf'), 5000));
    }, 

    


    addNewHydrant() {
        this.hydrantAddButton.click();
        this.waitForHydrantShelfOpen();  
    },

    setHydrantID(text){
        EsoControls.EsoText.setEsoTextField(FieldRefs.hydrantID, text); 
    },

    setHydrantLocationDescription(text){
        EsoControls.EsoText.setEsoTextField(FieldRefs.hydrantLocationDescription, text); 
    },

    setHydrantCrossStreet(text){
        EsoControls.EsoText.setEsoTextField(FieldRefs.hydrantCrossStreet, text); 
    },

    setHydrantFlow(text){
        EsoControls.EsoText.setEsoTextField(FieldRefs.hydrantFlow, text); 
    },


    //OTHER SOURCES
    get otherSourcesAddButton(){
        return $("collection-with-attachments[field-ref='PROPERTYWATER_OTHERWATERSOURCESCOMPLEXCOLLECTIONWITHATTACHMENT'] grid-row.add grid-cell button");
    }, 

    get otherSourcesDoneButton(){
        this.waitForOtherSourcesShelfOpen();   //Make sure shelf is open to click button
        return element(by.buttonText("Done"));
    }, 

    get otherSourcesCancelButton(){
        this.waitForOtherSourcesShelfOpen();    //Make sure shelf is open to click button
        return element(by.buttonText("Cancel"));
    }, 

    waitForOtherSourcesShelfOpen() { 
        browser.wait(() => {return $("properties-shelf-panel").isDisplayed();})
        browser.wait(EC.elementToBeClickable($("image-field div div div.shelf-click-indicator"), 5000));
    },

    waitForOtherSourcesShelfClose() { 
        browser.wait(EC.invisibilityOf($('eso-shelf'), 5000));
    }, 


    addNewOtherSources() {
        this.otherSourcesAddButton.click();
        this.waitForOtherSourcesShelfOpen();  
    },

    setWaterSourceType(text){
        EsoControls.EsoText.setEsoTextField(FieldRefs.waterSourceType, text); 
    },

    setWaterSourceLocation(text){
        EsoControls.EsoText.setEsoTextField(FieldRefs.waterSourceLocation, text); 
    }


};