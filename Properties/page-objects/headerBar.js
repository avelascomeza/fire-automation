/** 
 * Header Bar page-object
 * */
let headerBar = {



    /**Checks if the Headerbar is displayed. 
     * Returns true or false 
     * */
    isDisplayed() {
        return $("header").isPresent();
    },

    get homeButton() { return $("section button.action-button.home-bg") },

    get mainMenu() { return $("section button.action-button.hamburger-bg") },

    get mainMenuCreateProperty() { return $("li[ng-click='modal.ok(\\'createProperty\\')']") },

    get mainMenuLogout() { return $("li[ng-click='modal.ok(\\'logout\\')']") },

    get esoLabel() { return $("section.apps-menu-container h1[ng-click='showAppsMenu($event)']") },

    get appTitle() { return $("span.app-name") },

    getTitleText() {  return $("span.app-name").getText() }

};

module.exports = headerBar;