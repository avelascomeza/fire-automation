const EsoControls = require("../eso-core-controls/esoControls");
const EC = protractor.ExpectedConditions;

var FieldRefs = {
            numberOrMilepost: "CREATEPROPERTY_NUMBERORMILEPOST",
            streetName: "CREATEPROPERTY_STREETORHIGHWAY",
            city: "CREATEPROPERTY_CITY",
            zipcode: "CREATEPROPERTY_ZIP",
            county: "CREATEPROPERTY_COUNTY",
            prefix: "CREATEPROPERTY_STREETPREFIX",
            streetType: "CREATEPROPERTY_STREETTYPE",
            suffix: "CREATEPROPERTY_STREETSUFFIX",
            state: "CREATEPROPERTY_STATE"
    };


let createPropertyDialogue = {

    isDisplayed() {
        return $("create-property-modal").isPresent();
    },

    setNumberOrMilePost(text) {

        EsoControls.EsoText.getEsoTextField(FieldRefs.numberOrMilepost).sendKeys(text);     
    },

    get numberOrMilepost() { return $("eso-text[field-ref='CREATEPROPERTY_NUMBERORMILEPOST'] input") },


    setPrefix(option) {
        EsoControls.EsoSingleSelect.getEsoSingleSelect(FieldRefs.prefix).click();
        EsoControls.EsoSingleSelect.setEsoSingleSelect(option);
    },


    get prefix() { return $("eso-single-select[field-ref='CREATEPROPERTY_STREETPREFIX']") },


    setStreetName(text) {

        EsoControls.EsoText.getEsoTextField(FieldRefs.streetName).sendKeys(text);     
    },

    get streetName() { return $("eso-text[field-ref='CREATEPROPERTY_STREETORHIGHWAY'] input") },


    setStreetType(option) {
        EsoControls.EsoSingleSelect.getEsoSingleSelect(FieldRefs.streetType).click();
        EsoControls.EsoSingleSelect.setEsoSingleSelect(option);
    },

    setStreetTypeSearch(option) {
        EsoControls.EsoSingleSelect.getEsoSingleSelect(FieldRefs.streetType).click();
        EsoControls.EsoSingleSelect.setEsoSingleSelectSearch(option);
    },

    get streetType() { return $("eso-single-select[field-ref='CREATEPROPERTY_STREETTYPE']") },


    setSuffix(option) {
        EsoControls.EsoSingleSelect.getEsoSingleSelect(FieldRefs.suffix).click();
        EsoControls.EsoSingleSelect.setEsoSingleSelect(option);
    },

    get suffix() { return $("eso-single-select[field-ref='CREATEPROPERTY_STREETSUFFIX']") },



    setCity(text) {

        EsoControls.EsoText.getEsoTextField(FieldRefs.city).sendKeys(text);     
    },

    get city() { return $("eso-text[field-ref='CREATEPROPERTY_CITY'] input") },


    setState(option) {
        EsoControls.EsoSingleSelect.getEsoSingleSelect(FieldRefs.state).click();
        EsoControls.EsoSingleSelect.setEsoSingleSelect(option);
    },

    setStateSearch(option) {
        EsoControls.EsoSingleSelect.getEsoSingleSelect(FieldRefs.state).click();
        EsoControls.EsoSingleSelect.setEsoSingleSelectSearch(option);
    },

    get state() { return $("eso-single-select[field-ref='CREATEPROPERTY_STATE']") },


    setZipcode(text) {

        EsoControls.EsoText.getEsoTextField(FieldRefs.zipcode).sendKeys(text);     
    },

    get zipcode() { return $("eso-text[field-ref='CREATEPROPERTY_ZIP'] input") },


    setCounty(text) {

        EsoControls.EsoText.getEsoTextField(FieldRefs.county).sendKeys(text);     
    },

    get county() { return $("eso-text[field-ref='CREATEPROPERTY_COUNTY'] input") },



    get cancelBtn() { return $("button[ng-click='modal.cancel()']")  },

    get createPropertyBtn() { return $("button[ng-click='createProperty(propertyModel)']") }

};

module.exports = createPropertyDialogue;