const EsoControls = require("../eso-core-controls/esoControls");
const EC = protractor.ExpectedConditions;

var FieldRefs = {
            //INFO
            address: "",
            propertyType: "PROPERTY_TYPEID",
            propertyStatus: "PROPERTY_STATUSID",
            propertyStartDate: "PROPERTY_STARTDATE",

            //DEMOGRAPHICS
            propertyDivision: "PROPERTY_DIVISIONID",
            propertyFireDistrictBattalion: "PROPERTY_FIREDISTRICTBATTALIONID",
            propertyInspectionDistrict: "PROPERTY_INSPECTIONDISTRICTID",
            propertyStation: "PROPERTY_STATIONID",
            propertyCensusTract: "PROPERTY_CENSUSTRACT",
            propertyPlanningZone: "PROPERTY_PLANNINGZONEID",

            //SPECIAL HAZARDS
            propertySpecialHazards: "PROPERTY_SPECIALHAZARDSCOMPLEXCOLLECTIONWITHATTACHMENT",

            //NOTES
            propertyNotes: "PROPERTY_NOTESCOMPLEXCOLLECTIONWITHATTACHMENT"
};

var Tabs = {
    
    map: "property.map",
    property: "property.property",
    contacts: "property.contacts",
    water: "property.water",
    attachments: "property.attachment",

};

var JumpTabs = {
    
    info: "info",
    demographics: "Demographics",
    specialHazards: "Special Hazards",
    notes: "Notes"

};







let propertyTab = {

    isDisplayed() {
        return $("main.eso-app-main.has-tab-nav").isPresent();
    },

    goToTab(tab) {
        $(`section.eso-app-tab-nav nav ul li[ui-sref='property.${tab}']`).click();

    },

    goToJumpTab(jumpTab) {


    },

    //Property Address
    setAddress() {

           
    },

    get AddressValue() {
        var numAndStreet = $("eso-address-summary div div div.display-value:nth-child(1)").getText();
        var cityStateZip = $("eso-address-summary div div div.display-value:nth-child(2)").getText();

        return (numAndStreet + " " + cityStateZip)
    },


    ////INFO JUMP TAB FIELDS

    //// Property Type
    //FieldName: property-property.info.typeId
    //DataType: Single Select
    //ListRef: PROPERTYTYPELIST
    //Constraints: None
    setPropertyType(option) {
        EsoControls.EsoSingleSelect.getEsoSingleSelect(FieldRefs.propertyType).click();
        EsoControls.EsoSingleSelect.setEsoSingleSelect(option);

    },

    setPropertyTypeSearch(option) {
        EsoControls.EsoSingleSelect.getEsoSingleSelect(FieldRefs.propertyType).click();
        EsoControls.EsoSingleSelect.setEsoSingleSelectSearch(option);
    },

    get propertyTypeValue(){


    },

    //// Property Status
    //FieldName: property-property.info.statusId
    //DataType: Single Select
    //ListRef: PROPERTYSTATUSTYPELIST
    //Constraints: None
    setPropertyStatus(option) {
        EsoControls.EsoSingleSelect.getEsoSingleSelect(FieldRefs.propertyStatus).click();
        EsoControls.EsoSingleSelect.setEsoSingleSelect(option);
    },

    setPropertyStatusSearch(option) {
        EsoControls.EsoSingleSelect.getEsoSingleSelect(FieldRefs.propertyStatus).click();
        EsoControls.EsoSingleSelect.setEsoSingleSelectSearch(option);

    },

    get propertyStatusValue(){

        
    },

    //Property Start Date
    //FieldName: property-property.info.startDate
    //DataType: datetime
    //Constraints: None     **Probably should be???**
    setPropertyStartDate() {


    },

    get propertyStartDateValue(){

        
    },


    ////DEMOGRAPHICS JUMP TAB FIELDS

    //Property Division
    //FieldName: property-property.demographic.divisionId
    //DataType: Single Select
    //ListRef: FIREDIVISIONLIST
    //Constraints: None
    setPropertyDivision(option) {
        EsoControls.EsoSingleSelect.getEsoSingleSelect(FieldRefs.propertyDivision).click();
        EsoControls.EsoSingleSelect.setEsoSingleSelect(option);

    },

    setPropertyDivisionSearch(option) {
        EsoControls.EsoSingleSelect.getEsoSingleSelect(FieldRefs.propertyDivision).click();
        EsoControls.EsoSingleSelect.setEsoSingleSelectSearch(option);

    },

    get propertyDivisionValue(){

        
    },

    //Property District/Battalion
    //FieldName: property-property.demographic.fireDistrictBattalionId
    //DataType: Single Select
    //ListRef: FIREDISTRICTLIST
    //Constraints: None
    setPropertyDistrictBattalion(option) {
        EsoControls.EsoSingleSelect.getEsoSingleSelect(FieldRefs.propertyFireDistrictBattalion).click();
        EsoControls.EsoSingleSelect.setEsoSingleSelect(option);

    },

    setPropertyDistrictBattalionSearch(option) {
        EsoControls.EsoSingleSelect.getEsoSingleSelect(FieldRefs.propertyFireDistrictBattalion).click();
        EsoControls.EsoSingleSelect.setEsoSingleSelectSearch(option);

    },

    get propertyDistrictBattalionValue(){
        
        
    },

    //Property Inspection District
    //FieldName: property-property.demographic.inspectionDistrictId
    //DataType: Single Select
    //ListRef: INSPECTIONDISTRICTLIST
    //Constraints: None
    setPropertyInspectionDistrict(option) {
        EsoControls.EsoSingleSelect.getEsoSingleSelect(FieldRefs.propertyInspectionDistrict).click();
        EsoControls.EsoSingleSelect.setEsoSingleSelect(option);

    },

    setPropertyInspectionDistrictSearch(option) {
        EsoControls.EsoSingleSelect.getEsoSingleSelect(FieldRefs.propertyInspectionDistrict).click();
        EsoControls.EsoSingleSelect.setEsoSingleSelectSearch(option);

    },

    get propertyInspectionDistrictValue(){

        
    },

    //Property Station
    //FieldName: property-property.demographic.stationId
    //DataType: Single Select
    //ListRef: FIRESTATIONLIST
    //Constraints: None
    setPropertyStation(option) {
        EsoControls.EsoSingleSelect.getEsoSingleSelect(FieldRefs.propertyStation).click();
        EsoControls.EsoSingleSelect.setEsoSingleSelect(option);

    },

    setPropertyStationSearch(option) {
        EsoControls.EsoSingleSelect.getEsoSingleSelect(FieldRefs.propertyStation).click();
        EsoControls.EsoSingleSelect.setEsoSingleSelectSearch(option);

    },

    get propertyStationValue(){
        
        
    },

    //Property Census Tract
    //FieldName: property-property.demographic.censusTract
    //DataType: number
    //Constraints:  "min": 0,
	//				"max": 9999.99,
	//				"precision": 2
    setPropertyCensusTract(text) {
        EsoControls.EsoText.setEsoTextField(FieldRefs.propertyCensusTract, text);  

    },

    get propertyCensusTractValue(){
        return $("eso-text[field-ref='PROPERTY_CENSUSTRACT'] input").getText();
        
    },

    //Property Planning Zone
    //FieldName: property-property.demographic.planningZoneId
    //DataType: Single Select
    //ListRef: PLANNINGZONELIST
    //Constraints: None
    setPropertyPlanningZone(option) {
        EsoControls.EsoSingleSelect.getEsoSingleSelect(FieldRefs.propertyPlanningZone).click();
        EsoControls.EsoSingleSelect.setEsoSingleSelect(option);

    },

    setPropertyPlanningZoneSearch(option) {
        EsoControls.EsoSingleSelect.getEsoSingleSelect(FieldRefs.propertyPlanningZone).click();
        EsoControls.EsoSingleSelect.setEsoSingleSelectSearch(option);

    },

    get propertyPlanningZoneValue(){

        
    },


    ////SPECIAL HAZARDS JUMP TAB FIELDS

    //Special Hazards Collection

    get specialHazardAddButton(){
        return $("collection-with-attachments[field-ref='PROPERTY_SPECIALHAZARDSCOMPLEXCOLLECTIONWITHATTACHMENT'] grid-row.add grid-cell button");
    },  
    
    waitForSpecialHazardShelfOpen() { 
        browser.wait(() => {return $("properties-shelf-panel").isDisplayed();})
        browser.wait(EC.elementToBeClickable($("image-field div div div.shelf-click-indicator"), 5000));
    },

    waitForSpecialHazardShelfClose() { 
        browser.wait(EC.invisibilityOf($('eso-shelf'), 5000));
    }, 


    addPropertySpecialHazard() {
        this.specialHazardAddButton.click();
        this.waitForSpecialHazardShelfOpen();
        
        
    },

    deletePropertySpecialHazard() {


    },

    editPropertySpecialHazard() {


    },

    get propertySpecialHazardValue(){

        
    },



    ////NOTES JUMP TAB FIELDS

    //Notes Collection
    addPropertyNote() {


    },

    deletePropertyNote() {


    },

    editPropertyNote() {


    },

    get propertyNoteValue(){

        
    }



};


module.exports = propertyTab;