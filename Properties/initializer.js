let EsoConfig = require("./eso.conf");


let Initializer = {

    loginToEso: function() {
        console.log("Logging in...");
        browser.ignoreSynchronization = true;       
        browser.driver.manage().window().maximize(); 
        browser.driver.get(EsoConfig.baseUrl).then(() => {
            $("#UserName").sendKeys(EsoConfig.login);
            $("#Password").sendKeys(EsoConfig.password);
            $("#AgencyLoginId").sendKeys(EsoConfig.domain);
            element(by.buttonText("Let's Go!")).click();
        })
    },


        loadPage: function (pageUrl) {
        browser.ignoreSynchronization = true;
        // Navigate to the current page.
        return browser.get(EsoConfig.baseUrl + pageUrl).then(() => {
            // wait for deferred bootstrap body to not be present
            $("body.deferred-bootstrap-loading").waitAbsent().then(() => {
                browser.ignoreSynchronization = false;
                browser.waitForAngular();
                
            });
        });
    },

    unloadPage: function () {
        // Remove local and session storage to begin with clean slate.
        browser.executeScript("window.sessionStorage.clear();");
        browser.executeScript("window.localStorage.clear();");
    }

    

};


/*
let Initializer = {
    loginToEso: function () {
        console.log("Logging in...");
        browser.ignoreSynchronization = true;
        browser.driver.get(EsoConfig.baseUrl).then(() => {
            $("#UserName").sendKeys(EsoConfig.login);
            $("#Password").sendKeys(EsoConfig.password);
            $("#AgencyLoginId").sendKeys(EsoConfig.domain);
            element(by.buttonText("Let's Go!")).click();
            $("legacy-silverlight-container").waitReady().then(function () {
                // Navigate to the default URL.
                browser.get(EsoConfig.baseUrl + EsoConfig.defaultPage).then(() => {
                    // wait for deferred bootstrap body to not be present
                    $("body.deferred-bootstrap-loading").waitAbsent().then(() => {
                        browser.ignoreSynchronization = false;
                        browser.waitForAngular();
                        // Clear the legacy silverlight error. 
                        $("generic-error").waitReady().then(() => {
                            element(by.buttonText("Continue working")).click();
                            console.log("Clearing Legacy Silverlight Error...");
                        });
                        $("generic-error").waitAbsent();
                    });
                });
            });
        });
    },

    loadPage: function (pageUrl) {
        browser.ignoreSynchronization = true;
        // Navigate to the current page.
        return browser.get(EsoConfig.baseUrl + pageUrl).then(() => {
            // wait for deferred bootstrap body to not be present
            $("body.deferred-bootstrap-loading").waitAbsent().then(() => {
                browser.ignoreSynchronization = false;
                browser.waitForAngular();
            });
        });
    },

    unloadPage: function () {
        // Remove local and session storage to begin with clean slate.
        browser.executeScript("window.sessionStorage.clear();");
        browser.executeScript("window.localStorage.clear();");
    }
//};
*/

module.exports = Initializer;