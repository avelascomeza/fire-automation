const headerBar = require(`${protractor.pageObjectDir}headerBar`);
const createPropertyDialogue = require(`${protractor.pageObjectDir}createPropertyDialogue`);
const RandomHelpers = require("../helpers/random-helpers");
const propertySearch = require(`${protractor.pageObjectDir}propertySearch`)


const EC = protractor.ExpectedConditions;

describe("Sprint 3: Property Search", function () {

describe("Fire Properties Issue 783 - Search", function () {


    beforeAll(function () {
        browser.wait(() => {return headerBar.homeButton.isPresent(); })
        .then(() => {
            headerBar.homeButton.click();       
            browser.wait(() => {return $("div.search-control").isPresent(); })
        })
    });


    describe("AUTO COMPLETE", function () {

        afterEach(function () {
             propertySearch.searchBarInput.clear();
        });


    it("Auto-complete produces results when the search DOES relate to records", done => {
            browser.wait(() => {return propertySearch.searchBarIsDisplayed(); })
            .then(() => {
                browser.wait(EC.elementToBeClickable(propertySearch.searchBarInput, 5000))
                .then(() => {
                    propertySearch.searchBarInput.sendKeys("au");
                  
                    browser.wait(EC.visibilityOf($("div.search-control input.autocomplete-is-searching"), 5000));
                    browser.wait(EC.invisibilityOf($("div.search-control input.autocomplete-is-searching"), 5000)); 
                        propertySearch.autocompleteResults.then(options => { expect(options.length).not.toBe(0);  } )
                })
             })
    done();
    });

    it("Auto-complete does not produce results when the search DOES NOT relate to any records", done => {
            browser.wait(() => {return propertySearch.searchBarIsDisplayed(); })
            .then(() => {
                browser.wait(EC.elementToBeClickable(propertySearch.searchBarInput, 5000))
                .then(() => {
                    propertySearch.searchBarInput.sendKeys("gxcvd");
                  
                    browser.wait(EC.visibilityOf($("div.search-control input.autocomplete-is-searching"), 5000));
                    browser.wait(EC.invisibilityOf($("div.search-control input.autocomplete-is-searching"), 5000)); 
                        propertySearch.autocompleteResults.then(options => { expect(options.length).toBe(0);  } )
                })
             })
    done();
    });


    it("Can click an 'Auto-complete' result and go to detail view", done => {
          browser.wait(() => {return propertySearch.searchBarIsDisplayed(); })
            .then(() => {
                browser.wait(EC.elementToBeClickable(propertySearch.searchBarInput, 5000))
                .then(() => {
                    propertySearch.searchBarInput.sendKeys("8607 Dawnridge");

                    browser.wait(EC.visibilityOf($("div.search-control input.autocomplete-is-searching"), 5000));
                    browser.wait(EC.invisibilityOf($("div.search-control input.autocomplete-is-searching"), 5000)); 


                  //  propertySearch.selectAutoCompleteAddress("8607 Dawnridge Circle");

                        propertySearch.autocompleteResults.then(options => { options[0].click();  } )   //RandomHelpers.randomInteger(options.length - 1)

                    browser.wait(() => {return $("main.eso-app-main.has-tab-nav").isPresent(); })
                    .then(() => {
                        headerBar.homeButton.click();
                        browser.wait(() => { return propertySearch.searchBar.isPresent(); })
                    })
                })
             })  
    done();
    });


});


    describe("SEARCH RESULTS", function () {

     //   beforeAll(function () {
     //       browser.wait(EC.invisibilityOf($("main.eso-app-main.has-tab-nav"), 5000));
     //   })

        beforeEach(function () {
            browser.wait(EC.elementToBeClickable(propertySearch.searchBarInput, 5000));
             propertySearch.searchBarInput.clear();
             propertySearch.searchBarInput.sendKeys("ghj");
             propertySearch.searchBarInput.clear();
        });

        afterEach(function () {
             propertySearch.searchBarInput.clear();
        });


        //BEFORE:  Need to be looking at landing page with search bar present
            //PASS: 'Search Results shelf' displays after hitting 'Enter'
            //FAIL: 'Search Results shelf' doesn't display after hitting 'Enter'
        //AFTER:   Need to be looking at landing page with search bar present

    it("Hitting 'enter' after a search displays the 'search results shelf'", done => {
            browser.wait(() => {return propertySearch.searchBarIsDisplayed(); })
            .then(() => {
                browser.wait(EC.elementToBeClickable(propertySearch.searchBarInput, 5000))
                .then(() => {
                    propertySearch.searchBarInput.sendKeys("au");
                  
                //    browser.wait(EC.visibilityOf($("div.search-control input.autocomplete-is-searching"), 5000));
                //    browser.wait(EC.invisibilityOf($("div.search-control input.autocomplete-is-searching"), 5000)); 

                        browser.actions().sendKeys(protractor.Key.ENTER).perform();

                    browser.wait(EC.invisibilityOf($("div.sidebar div.search-results-container.ng-hide"), 5000));
                    browser.wait(EC.visibilityOf($("div.sidebar div.search-results-container"), 5000));

                    browser.wait(EC.elementToBeClickable($("div.cancel-icon"), 5000));

                    $("div.cancel-icon").click();


                })
             })
    
    done();
    });

    it("Clicking 'search' after a search displays the 'search results shelf'", done => {
            browser.wait(() => {return propertySearch.searchBarIsDisplayed(); })
            .then(() => {
                browser.wait(EC.elementToBeClickable(propertySearch.searchBarInput, 5000))
                .then(() => {
                    propertySearch.searchBarInput.sendKeys("au");
                  
             //       browser.wait(EC.visibilityOf($("div.search-control input.autocomplete-is-searching"), 5000));
             //       browser.wait(EC.invisibilityOf($("div.search-control input.autocomplete-is-searching"), 5000)); 

                        propertySearch.searchButton.click();

                    browser.wait(EC.invisibilityOf($("div.sidebar div.search-results-container.ng-hide"), 5000));
                    browser.wait(EC.visibilityOf($("div.sidebar div.search-results-container"), 5000));

                    browser.wait(EC.elementToBeClickable($("div.cancel-icon"), 5000));
                    $("div.cancel-icon").click();
                })
             })


    
    done();
    });

    it("A search that has no results displays 'No results found'", done => {
            browser.wait(() => {return propertySearch.searchBarIsDisplayed(); })
            .then(() => {
                browser.wait(EC.elementToBeClickable(propertySearch.searchBarInput, 5000))
                .then(() => {
                    propertySearch.searchBarInput.sendKeys("ghjazcbd");
                  
                //    browser.wait(EC.visibilityOf($("div.search-control input.autocomplete-is-searching"), 5000));
                //    browser.wait(EC.invisibilityOf($("div.search-control input.autocomplete-is-searching"), 5000)); 

                        browser.actions().sendKeys(protractor.Key.ENTER).perform();

                    browser.wait(EC.invisibilityOf($("div.sidebar div.search-results-container.ng-hide"), 5000));
                    browser.wait(EC.visibilityOf($("div.sidebar div.search-results-container"), 5000));

                        propertySearch.searchResults.then(options => { expect(options.length).toBe(0);  } )
                })
             })   
    done();
    });    

    it("The 'search results shelf' shows and distinguishes 'non-validated' results with a 'not mapped' tag", done => {
            browser.wait(() => {return propertySearch.searchBarIsDisplayed(); })
            .then(() => {
                browser.wait(EC.elementToBeClickable(propertySearch.searchBarInput, 5000))
                .then(() => {
                    propertySearch.searchBarInput.sendKeys("8607 dawnridge");
                  
               //     browser.wait(EC.visibilityOf($("div.search-control input.autocomplete-is-searching"), 5000));
               //     browser.wait(EC.invisibilityOf($("div.search-control input.autocomplete-is-searching"), 5000)); 

                        browser.actions().sendKeys(protractor.Key.ENTER).perform();

                    browser.wait(EC.invisibilityOf($("div.sidebar div.search-results-container.ng-hide"), 5000));
                    browser.wait(EC.visibilityOf($("div.sidebar div.search-results-container"), 5000));
                    browser.wait(EC.visibilityOf($("div.not-mapped"), 5000));
                   

                       $$("div.not-mapped").then(options => { expect(options.length).not.toBe(0);  })
                })
             })




    
    done();
    });

    it("Clicking a result in the 'search results shelf' takes the user to the detail view for that result", done => {
            browser.wait(() => {return propertySearch.searchBarIsDisplayed(); })
            .then(() => {
                browser.wait(EC.elementToBeClickable(propertySearch.searchBarInput, 5000))
                .then(() => {
                    propertySearch.searchBarInput.sendKeys("8607 dawnridge");
                  
                //    browser.wait(EC.visibilityOf($("div.search-control input.autocomplete-is-searching"), 5000));
                //    browser.wait(EC.invisibilityOf($("div.search-control input.autocomplete-is-searching"), 5000)); 

                        browser.actions().sendKeys(protractor.Key.ENTER).perform();

                    browser.wait(EC.invisibilityOf($("div.sidebar div.search-results-container.ng-hide"), 5000));
                    browser.wait(EC.visibilityOf($("div.sidebar div.search-results-container"), 5000));

                       propertySearch.searchResults.then(options => { options[0].click();  } )

                       browser.wait(() => {return $("main.eso-app-main.has-tab-nav").isPresent(); })
                       .then(() => {
                        headerBar.homeButton.click();
                        browser.wait(() => { return propertySearch.searchBar.isPresent(); })
                    })


                })
             })
    
    done();
    });



    });

});

});

/*
    it("Search Produces results of type 'Address' ", done => {
            
    
    done();
});
*/
   

    

/*
    it("Search results only derive from the current Agency's database", done => {
            
    
    done();
});
*/