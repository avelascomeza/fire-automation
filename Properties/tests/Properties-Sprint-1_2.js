const headerBar = require(`${protractor.pageObjectDir}headerBar`);
const createPropertyDialogue = require(`${protractor.pageObjectDir}createPropertyDialogue`);
const RandomHelpers = require("../helpers/random-helpers");
const TestData = require("../helpers/test-data.js");
const propertySearch = require(`${protractor.pageObjectDir}propertySearch`)

const EC = protractor.ExpectedConditions;

describe("Sprint 1/2: Header bar, Create Property", function () {



describe("Fire Properties Issue 946 - Header Bar", function () {

    it("Can see and click the 'Home' Button", done => {      

        browser.wait(() => {return headerBar.homeButton.isPresent(); })
        .then(() => { headerBar.homeButton.click(); })
        
    done();
    });

/** 
 * GIVEN I am any user
 * AND I am looking at the landing page
 * AND I click the ESO Label in the header bar
 * THEN I should see links to all apps in ESO Suite
 * */
     it("Can see and click the 'ESO Label' button", done => {
            
           browser.wait(() =>  headerBar.esoLabel.isPresent() )
           .then(() => { 
               headerBar.esoLabel.click(); 
               //Remove the veil
               $("veil").click();
           })
    done();
    });

    it("Can see and click the 'Main Menu' button", done => {
            
           browser.wait(() => {return headerBar.mainMenu.isPresent(); })
           .then(() => { 
               headerBar.mainMenu.click(); 
               //Remove the veil
               $("veil").click();
           })
    done();
    });


    it("'Main Menu' has options: 'Create Property' and 'Logout'", done => {
            
           browser.wait(() => {return headerBar.mainMenu.isPresent(); })
           .then(() => { 
               headerBar.mainMenu.click();              
           })
           .then(() => {
                expect(headerBar.mainMenuCreateProperty.isPresent()).toBe(true);
                expect(headerBar.mainMenuLogout.isPresent()).toBe(true);
                //Remove the veil
               $("veil").click();
           })
    done();
    });

    it("App title is 'Fire-Properties'", done => {

           browser.wait(() => { return headerBar.appTitle.isPresent();  })
           .then(() => { 
                expect(headerBar.getTitleText()).toMatch("FIRE - PROPERTIES");       
           })
    done();
    });
});


describe("Fire Properties Issue 777 - Create Property", function () {

    it("Can click 'Create Property' in the 'Search bar'", done => {

        browser.wait(() => {return $("div.search-control").isPresent(); })
        .then(() => {
            propertySearch.setSearch("au");
            propertySearch.createProperty();

            browser.wait(() => {return createPropertyDialogue.isDisplayed(); })               
            .then(() => {
                browser.wait(EC.elementToBeClickable(createPropertyDialogue.cancelBtn, 5000));              
                createPropertyDialogue.cancelBtn.click();
            })                           
        })
    done();
    });


    it("Can click 'Create Property' in 'Main Menu'", done => {

           browser.wait(() => {return headerBar.mainMenu.isPresent(); })
           .then(() => {
                headerBar.mainMenu.click();
           })
           .then(() => {
                headerBar.mainMenuCreateProperty.click();
                createPropertyDialogue.isDisplayed();
           })

        done();
    });

   
    it("Check that 'Mandatory' fields are marked", done => {

       browser.wait(() => {return createPropertyDialogue.isDisplayed(); })
       .then(() => {
            expect($("eso-field[field-label='\\'Number or Milepost\\''] div div div.tag-text.red").isPresent()).toBe(true);
            expect($("eso-field[field-label='\\'Street Name\\''] div div div.tag-text.red").isPresent()).toBe(true);
            expect($("eso-field[field-label='\\'City\\''] div div div.tag-text.red").isPresent()).toBe(true);
            expect($("eso-field[field-label='\\'State\\''] div div div.tag-text.red").isPresent()).toBe(true);
            expect($("eso-field[field-label='\\'Zip\\''] div div div.tag-text.red").isPresent()).toBe(true);
       })


       done(); 
    });


 it("Cannot create a property without 'Mandatory' fields", done => {

           browser.wait(() => {return createPropertyDialogue.isDisplayed(); })
           .then(() => {
                createPropertyDialogue.numberOrMilepost.sendKeys("8607");
                expect(createPropertyDialogue.createPropertyBtn.getAttribute('disabled')).toBe('true');
                createPropertyDialogue.numberOrMilepost.clear();

                createPropertyDialogue.streetName.sendKeys("street");
                expect(createPropertyDialogue.createPropertyBtn.getAttribute('disabled')).toBe('true');
                createPropertyDialogue.streetName.clear();

                
           })
          
        done();
    });



    it("Cannot create a property without a UNIQUE address", done => {

           browser.wait(() => {return createPropertyDialogue.isDisplayed(); })
           .then(() => {

                createPropertyDialogue.setNumberOrMilePost("8611");

                createPropertyDialogue.setPrefix(3);

                createPropertyDialogue.setStreetName("Dawnridge");

                createPropertyDialogue.setStreetType(3);

                createPropertyDialogue.setSuffix(3);

                createPropertyDialogue.setCity("Austin"); 
              
                createPropertyDialogue.setState(3);

                createPropertyDialogue.setZipcode("78757"); 

                createPropertyDialogue.setCounty("Travis");



                createPropertyDialogue.createPropertyBtn.click();

                browser.wait(() => {return $("eso-alert-modal-dialog").isPresent(); })
                .then(() => {                   
                    browser.wait(EC.elementToBeClickable($("button[ng-click='modal.ok()']")), 5000);
                    $("button[ng-click='modal.ok()']").click();
                    browser.wait(EC.invisibilityOf($('eso-alert-modal-dialog'), 5000));
                })

           })
          
        done();
    });


    it("Can create a property with GOOD data in all fields and is UNIQUE", done => {
        browser.wait(() => {return createPropertyDialogue.isDisplayed(); })
        .then(() => {
            createPropertyDialogue.numberOrMilepost.clear();
            createPropertyDialogue.numberOrMilepost.sendKeys(RandomHelpers.randomInteger(999999).toString());

            createPropertyDialogue.createPropertyBtn.click();

                browser.wait(() => {return $("main.eso-app-main.has-tab-nav").isPresent(); })    
                .then(() => {
                    headerBar.homeButton.click();
                    browser.wait(() => {return $("div.search-control").isPresent(); })  
                })

        })

    done();
    });


});


});