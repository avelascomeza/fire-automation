const headerBar = require(`${protractor.pageObjectDir}headerBar`);
const createPropertyDialogue = require(`${protractor.pageObjectDir}createPropertyDialogue`);
const RandomHelpers = require("../helpers/random-helpers");
const propertySearch = require(`${protractor.pageObjectDir}propertySearch`);
const EsoControls = require("../eso-core-controls/esoControls");

const EC = protractor.ExpectedConditions;


const buildingName = "building58";
const occupantName = "occupant58";


describe("Sprint 4: Building, Occupant, Record Nav ", function () {

describe("Fire Properties Issue 981 - Add Building", function () {

beforeAll(function () {

         
            browser.wait(() => {return headerBar.homeButton.isPresent(); })
            .then(() => {
                headerBar.homeButton.click();       
                browser.wait(() => {return $("div.search-control").isPresent(); })
                .then(() => {
                    browser.wait(EC.elementToBeClickable(propertySearch.searchBarInput, 5000));
                    propertySearch.searchBarInput.clear();
                    propertySearch.searchBarInput.sendKeys("8607");
                    browser.wait(EC.visibilityOf($("div.search-control input.autocomplete-is-searching"), 5000));
                    browser.wait(EC.invisibilityOf($("div.search-control input.autocomplete-is-searching"), 5000));
                
                    browser.wait(() => {return $("div.property-autocomplete ul li").isPresent(); })
                    .then(() => {
                         $("div.property-autocomplete ul li").click();
                    })
                })
            })
});


   it("User can click on 'property label' and get to 'property navigation' drop down", done => {

    done();
});

it("User can can see buildings attached to property in the 'property navigation' drop down", done => {

    done();
});

it("User cannot click 'create building' until required fields are filled", done => {

    done();
});

it("Building start date defaults to system date", done => {

    done();
    });

it("User can click 'cancel' and go back to property navigation drop down", done => {

    done();
});




    it("A User with permission can add a building to a property", done => {

        browser.wait(() => {return $("main.eso-app-main.has-tab-nav").isPresent(); })
        .then(() => {
            $("property-header section.property").click();
            browser.wait(() => { return $("div.eso-popover.show").isPresent(); })
            .then(() => {
                element(by.buttonText("New Building")).click();
                    browser.wait(() => {return $("create-building-modal").isPresent(); })
                    .then(() => {
                            browser.wait(EC.elementToBeClickable(EsoControls.EsoText.getEsoTextField("CREATEBUILDING_NAMEORNUMBER"), 5000));
                            EsoControls.EsoText.getEsoTextField("CREATEBUILDING_NAMEORNUMBER").sendKeys(buildingName);
                            element(by.buttonText("Create Building")).click();
                            browser.wait(EC.invisibilityOf($("create-building-modal"), 5000));
                            browser.wait(EC.elementToBeClickable(element(by.buttonText("New Occupant")), 5000));
                     })   
            })
        })


    done();
});

 

it("User cannot create a 'duplicate building' and if tried, is displayed a warning message", done => {

            browser.wait(() => { return $("div.eso-popover.show").isPresent(); })
            .then(() => {
                element(by.buttonText("New Building")).click();
                    browser.wait(() => {return $("create-building-modal").isPresent(); })
                    .then(() => {
                            browser.wait(EC.elementToBeClickable(EsoControls.EsoText.getEsoTextField("CREATEBUILDING_NAMEORNUMBER"), 5000));
                            EsoControls.EsoText.getEsoTextField("CREATEBUILDING_NAMEORNUMBER").sendKeys(buildingName);
                            element(by.buttonText("Create Building")).click();
                            browser.wait(() => {return $("eso-alert-modal-dialog").isPresent(); })
                            .then(() => {
                                browser.wait(EC.elementToBeClickable(element(by.buttonText("OK")), 5000));
                                element(by.buttonText("OK")).click(); 

                                browser.wait(EC.invisibilityOf($("eso-modal[data-order='3']"), 5000));    
                                                      
                     
                                    browser.wait(EC.elementToBeClickable(element(by.buttonText("Cancel")))); 
                                    element(by.buttonText("Cancel")).click();
                
                                browser.wait(EC.invisibilityOf($("create-building-modal"), 5000));     
                                browser.wait(EC.elementToBeClickable(element(by.buttonText("New Occupant")), 5000));
                            }) 
                    })   
            })
        
    done();
    });
});


describe("Fire Properties Issue 985 - Add Occupant", function () {

 

it("User can see occupants attached to buildings in the 'property navigation' drop down", done => {

    done();
});

it("User cannot click 'create occupant' until required fields are filled", done => {

    done();
});

it("Occupant start date adheres to constraints", done => {

    done();
    });

it("User can click 'cancel' and go back to property navigation drop down", done => {

    done();
});




    it("User with permission can add an occupant", done => {

            browser.wait(() => { return $("div.eso-popover.show").isPresent(); })
            .then(() => {
                browser.wait(EC.elementToBeClickable(element(by.buttonText("New Occupant")), 5000));
                element(by.buttonText("New Occupant")).click();
                browser.wait(() => { return $("create-occupant-modal").isPresent(); })
                .then(() => {
                    browser.wait(EC.elementToBeClickable($("eso-text[ng-model='model.occupantName'] input"), 5000));
                    $("eso-text[ng-model='model.occupantName'] input").sendKeys(occupantName)

                    $("eso-text[ng-model='model.suiteNumber'] input").sendKeys("suite1")

                        //Defaults to a building now
                    $("eso-single-select[ng-model='model.buildingId']").click();
                    browser.wait(() => {return $("eso-single-select-shelf").isDisplayed(); })               
                    .then(() => {
                                          
                        $("eso-search-input input").sendKeys(buildingName);
                        $$("eso-single-select-panel ul li").then(options => { options[0].click();  } )
                  //      browser.wait(EC.invisibilityOf($('veil'), 5000));
      



                   //     $$("eso-single-select-panel ul li").then(options => { options[0].click();  } )
                   //     browser.wait(EC.invisibilityOf($('veil'), 5000));
                  //      browser.wait(EC.invisibilityOf($("eso-single-select-shelf"), 5000));

                    
                   //     EsoControls.EsoSingleSelect.setEsoSingleSelectSearch(buildingName);

                        browser.wait(EC.invisibilityOf($("eso-shelf[data-order='3']"), 5000));
                     //   browser.wait(EC.invisibilityOf($("eso-single-select-shelf"), 5000));

                        browser.wait(EC.elementToBeClickable(element(by.buttonText("Create Occupant")), 5000));
                        element(by.buttonText("Create Occupant")).click();

                        browser.wait(EC.invisibilityOf($("create-occupant-modal"), 5000));
                        browser.wait(EC.elementToBeClickable(element(by.buttonText("New Occupant")), 5000));

                     })

                })
            })
        
    done();
});

it("User cannot create duplicate occupant", done => {

            browser.wait(() => { return $("div.eso-popover.show").isPresent(); })
            .then(() => {
                browser.wait(EC.elementToBeClickable(element(by.buttonText("New Occupant")), 5000));
                element(by.buttonText("New Occupant")).click();
                browser.wait(() => { return $("create-occupant-modal").isPresent(); })
                .then(() => {
                    browser.wait(EC.elementToBeClickable($("eso-text[ng-model='model.occupantName'] input"), 5000));
                    $("eso-text[ng-model='model.occupantName'] input").sendKeys(occupantName)

                    $("eso-text[ng-model='model.suiteNumber'] input").sendKeys("suite1")

                    $("eso-single-select[ng-model='model.buildingId']").click();
                    browser.wait(() => {return $("eso-single-select-shelf").isDisplayed(); })               
                    .then(() => {

                    //    $$("eso-single-select-panel ul li").then(options => { options[0].click();  } )
                     //   browser.wait(EC.invisibilityOf($('veil'), 5000));
                    //    browser.wait(EC.invisibilityOf($("eso-single-select-shelf"), 5000));

                        $("eso-search-input input").sendKeys(buildingName);
                        $$("eso-single-select-panel ul li").then(options => { options[0].click();  } )

                    //    EsoControls.EsoSingleSelect.setEsoSingleSelectSearch(buildingName);

                        browser.wait(EC.invisibilityOf($("eso-shelf[data-order='3']"), 5000));

                    //    browser.wait(() => {return EC.invisibilityOf($("eso-single-select-shelf"), 5000); })
                     //   .then(() => {

                        

                        browser.wait(EC.elementToBeClickable(element(by.buttonText("Create Occupant")), 5000));
                        element(by.buttonText("Create Occupant")).click();
                    
                    
                        browser.wait(() => {return $("eso-alert-modal-dialog").isPresent(); })
                            .then(() => {
                                browser.wait(EC.elementToBeClickable(element(by.buttonText("OK")), 5000));
                                element(by.buttonText("OK")).click(); 

                         //       browser.wait(EC.invisibilityOf($("eso-alert-modal-dialog"), 5000));                          
                         //       browser.wait(() => {return EC.elementToBeClickable(element(by.buttonText("Cancel")), 5000); })
                         //       .then(() => {
                         //           element(by.buttonText("Cancel")).click();
                         //       }) 
                             //   browser.wait(EC.invisibilityOf($("eso-alert-modal-dialog"), 5000));    

                                browser.wait(EC.invisibilityOf($("eso-modal[data-order='3']"), 5000));   

                            //    browser.wait(() => {return EC.invisibilityOf($("div.veil"), 5000); })
                           //     .then(() => {
                                    browser.wait(EC.elementToBeClickable(element(by.buttonText("Cancel")))); 
                                    element(by.buttonText("Cancel")).click();
                            //    }) 

                                browser.wait(EC.invisibilityOf($("create-occupant-modal"), 5000));     
                                browser.wait(EC.elementToBeClickable(element(by.buttonText("New Occupant")), 5000));
                            }) 
                    //    })
                     })

                })
            })
        
    done();
    });



});


//describe("Fire Properties Issue 985 B - Occupant Search", function () {




//});


describe("Fire Properties Issue 979 - Detail Record Nav", function () {

it("Can search for building in property navigation drop down", done => {

    done();
});

it("Can search for occupant in property navigation drop down", done => {

    done();
});


});

});