const headerBar = require(`${protractor.pageObjectDir}headerBar`);
const createPropertyDialogue = require(`${protractor.pageObjectDir}createPropertyDialogue`);
const RandomHelpers = require("../helpers/random-helpers");
const TestData = require("../helpers/test-data.js");
const propertySearch = require(`${protractor.pageObjectDir}propertySearch`)
const EC = protractor.ExpectedConditions;


//Create a Property

////TODO: Detect if the needed properties already exis

describe("Create Test Properties needed for regression", function () {
    //Ensure page has loaded
    beforeAll(function () {
        browser.wait(() => {return headerBar.homeButton.isPresent(); })
    });

    //Go back to home page and open create property dialog
    beforeEach(function () {
 
        browser.wait(() => {return headerBar.homeButton.isPresent(); })
        .then(() => {
            headerBar.homeButton.click();     
            browser.wait(() => {return $("div.search-control").isPresent(); })
            .then(() => {
                headerBar.mainMenu.click();
                browser.wait(() => {return headerBar.mainMenuCreateProperty.isPresent(); })   
                .then(() => {
                    headerBar.mainMenuCreateProperty.click();
                })
            })
        }) 
    });

    afterAll(function () {
        browser.wait(() => {return headerBar.homeButton.isPresent(); })
        .then(() => {
            headerBar.homeButton.click();       
            browser.wait(() => {return $("div.search-control").isPresent(); })
        })
    });

    //Click Main Menu
    //Click Create Property
    //Fill info
    //Click Create Property
    it("(Sprint 2 - Create Property) Creates test property '8611 NW Dawnridge Circle NW'", done => {  
        browser.wait(() => {return createPropertyDialogue.isDisplayed(); })   
        .then(() => {
            browser.wait(EC.elementToBeClickable(createPropertyDialogue.cancelBtn, 5000));   
            createPropertyDialogue.setNumberOrMilePost("8611");
            createPropertyDialogue.setPrefix(3);   //Northwest
            createPropertyDialogue.setStreetName("Dawnridge");
            createPropertyDialogue.setStreetType(3);   //Avenue
            createPropertyDialogue.setSuffix(3);   //Northwest
            createPropertyDialogue.setCity("Austin");              
            createPropertyDialogue.setState(3);   //American Samoa
            createPropertyDialogue.setZipcode("78757"); 
            createPropertyDialogue.setCounty("Travis");
            createPropertyDialogue.createPropertyBtn.click();

            browser.wait(() => {return $("main.eso-app-main.has-tab-nav").isPresent(); })
            .then(()=> {
                expect($("main.eso-app-main.has-tab-nav").isPresent()).toBe(true);
            })        
        })           
    done();
    }); // End it()

    //Click Main Menu
    //Click Create Property
    //Fill info
    //Click Create Property
    it("(Sprint 3/4 - Search/Building and Occupant) Creates test property '8607 Dawnridge Circle'", done => {  
        browser.wait(() => {return createPropertyDialogue.isDisplayed(); }) 
        .then(() => {
            browser.wait(EC.elementToBeClickable(createPropertyDialogue.cancelBtn, 5000)); 
            createPropertyDialogue.setNumberOrMilePost("8607");
            createPropertyDialogue.setStreetName("Dawnridge");
            createPropertyDialogue.setStreetTypeSearch("Circle");    //Circle
            createPropertyDialogue.setCity("Austin");              
            createPropertyDialogue.setStateSearch("Texas");   //Texas
            createPropertyDialogue.setZipcode("78757"); 
            createPropertyDialogue.setCounty("Travis");
            createPropertyDialogue.createPropertyBtn.click();

            browser.wait(() => {return $("main.eso-app-main.has-tab-nav").isPresent(); })
            .then(()=> {
                expect($("main.eso-app-main.has-tab-nav").isPresent()).toBe(true);
            }) 
        })           
    done();
    }); // End it()

}); //End describe()
