const headerBar = require(`${protractor.pageObjectDir}headerBar`);
const createPropertyDialogue = require(`${protractor.pageObjectDir}createPropertyDialogue`);
const RandomHelpers = require("../helpers/random-helpers");
const propertySearch = require(`${protractor.pageObjectDir}propertySearch`);
const propertyTab = require(`${protractor.pageObjectDir}propertyTab`);
const EsoControls = require("../eso-core-controls/esoControls");

const EC = protractor.ExpectedConditions;

const path = require('path');



describe("Sprint 5: Collection Attachments", function () {

describe("Fire Properties Issue 1686 - Collection Attachments", function () {

    beforeAll(function () {

         
            browser.wait(() => {return headerBar.homeButton.isPresent(); })
            .then(() => {
                headerBar.homeButton.click();       
                browser.wait(() => {return $("div.search-control").isPresent(); })
                .then(() => {
                    browser.wait(EC.elementToBeClickable(propertySearch.searchBarInput, 5000));
                    propertySearch.searchBarInput.clear();
                    propertySearch.searchBarInput.sendKeys("8607");
                    browser.wait(EC.visibilityOf($("div.search-control input.autocomplete-is-searching"), 5000));
                    browser.wait(EC.invisibilityOf($("div.search-control input.autocomplete-is-searching"), 5000));
                
                    browser.wait(() => {return $("div.property-autocomplete ul li").isPresent(); })
                    .then(() => {
                         $("div.property-autocomplete ul li").click();

                         browser.wait(() => {return propertyTab.isDisplayed(); })
                         .then(() => {
                             propertyTab.goToTab("property");
                             browser.wait(() => {return $("property-tab").isPresent(); })
                             browser.wait(EC.invisibilityOf($("div.eso-loading-cover"), 5000));
                         })
                    })
                 

                })
            })

});

     it("User can upload a jpg", done => {
            
            propertyTab.addPropertySpecialHazard();
            $("image-field div div div.shelf-click-indicator").click();
        
            var fileToUpload = 'C:/Users/chris.britton/Desktop/PNbL9Jv.png',
            absolutePath = path.resolve(__dirname, fileToUpload);

            var form = element(By.css('form'));

            element(by.css('input[type="file"]')).sendKeys(absolutePath);  

            browser.wait(() => {return $("div.loading-spinner-bg").isPresent(); }, 5000)
            .then(() => {
                browser.wait(EC.invisibilityOf($("div.loading-spinner-bg"), 5000));

               element(by.buttonText("Done")).click();
               // form.submit();
               

              //  form = element(By.css('form'));
           //     form.sendKeys(protractor.Key.ESCAPE);
           
            })
       
    

        done();
    });       









});


});







describe("Sprint 6: Property Tab", function () {

describe("Fire Properties Issue 1627 - Property Tab", function () {




});

});




