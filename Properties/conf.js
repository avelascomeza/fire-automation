// conf.js
exports.config = {
    framework: "jasmine",
    seleniumAddress: "http://localhost:4444/wd/hub",
    //seleniumAddress: "http://hub.browserstack.com/wd/hub/",

    specs: [
     //   "tests/Regression-Setup.js",
     //   "tests/Properties-Sprint-1_2.js",
     //   "tests/Properties-Sprint-3.js",
    //    "tests/Properties-Sprint-4.js"
        "tests/Properties-Sprint-5_6.js"
   //     "tests/Properties-Sprint-7.js"
    ],

    onPrepare: function () {

        // Load our helper libraries, these add some custom extension methods.
        require("./helpers/wait-absent.js");
        require("./helpers/wait-ready.js");
        require("./helpers/date-helpers.js");

        // The Initializer, which handles ESO-centric methods of loading pages.
        const Initializer = require("./initializer");

        // Add our custom reporters.
        const SpecReporter = require("jasmine-spec-reporter").SpecReporter;
        const HtmlReporter = require("protractor-beautiful-reporter");

        // Adds a Spec reporter that outputs to console.
        jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));

        // Add a screenshot reporter and store screenshots to `/tmp/screenshots`: 
        jasmine.getEnv().addReporter(new HtmlReporter({
            baseDirectory: `./reports/${new Date().format("{FullYear}{Month:2}{Date:2}-{Hours:2}{Minutes:2}{Seconds:2}")}`,
            screenshotsSubfolder: "images",
            jsonsSubfolder: "json",
            docName: "report.html",
            docTitle: "Fire-Properties Regression Tests",
            takeScreenShotsOnlyForFailedSpecs: true
        }).getJasmine2Reporter());

        // Define base path in order to load Page Objects.
       
        protractor.baseDir = __dirname + "/";
        protractor.pageObjectDir = protractor.baseDir + "page-objects/";

        // Log in to the ESO Suite. Return makes it async, preventing us from 
        // starting tests until the promise resolves.
        return Initializer.loginToEso();
    },

    // Browser capabilities.
    multiCapabilities: [

      //  {
      //    browserName: 'firefox',
      //    acceptInsecureCerts: true
      //  }

        // ,

         {
             browserName: "chrome",
             'chromeOptions': {
                 args: ["--test-type", "--ignore-certificate-errors"]
             }
         }

    ],

    // Options to be passed to Jasmine-node.
    jasmineNodeOpts: {
        print: function () { /* Prevent the default spec dots from appearing in the output */ }
    }

}