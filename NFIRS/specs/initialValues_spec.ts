import { FieldType } from '../Enums/enums';
import { Tab } from '../pagenavigationcontroller';
import { Field } from '../Types/Field';
import { FieldsetCompareResult, FieldValue } from '../Types/fieldValue';
import { ValidationRuleController } from '../Types/validationrulecontroller';
import { ValueController } from '../Types/ValueController';

import * as console from 'console';
import { readFileSync } from 'fs';

describe("INITIALVALUES", function ()
{

    it("Verifies the initial values have not changed.", done =>
    {

        ValueController.LoadInitialValues().then(() =>
        {
            var current = ValueController.CurrentFieldValues;

            var semiParsed: any[] = JSON.parse(readFileSync("./Data/initialvalues.json", 'utf8'));

            var original: FieldValue[] = [];
            semiParsed.forEach(item =>
            {
                var field = new Field(item.FieldRef, item.FieldType, item.Tab)
                field.FieldLabel = item.FieldLabel;
                field.IsRequired = item.IsRequired;
                field.IsEnabled = item.IsEnabled;

                var val = new FieldValue(field, item.Value);
                original.push(val);


            });


            //set the fields we expect to be different
            var fieldsToSkip: string[] = ['INCIDENTNUMBER', 'NFIRSINCIDENTNUMBER']

            var typesToSkip: FieldType[] = [FieldType.DateTime, FieldType.Time];



            var valueCompare: FieldsetCompareResult = ValueController.CompareValueSets(original, current, fieldsToSkip, typesToSkip,false,false);




            expect(valueCompare.MissingValues.length).toBe(0, 'Some values where not found: ' + JSON.stringify(valueCompare.MissingValues));
            expect(valueCompare.NewValues.length).toBe(0, 'New Values Were found: ' + JSON.stringify(valueCompare.NewValues));
            expect(valueCompare.ChangedValues.length).toBe(0, 'Initial Values Changed: ' + JSON.stringify(valueCompare.ChangedValues));

            done();
        });
    });


    it("Verifies validation rule count matches rows.", done =>
    {
        ValidationRuleController.GetCurrentValidationRuleCount().then((num) =>
        {
            expect(num).toBe("34", num + " validation rules where found, only 34 where expected");



            ValidationRuleController.GetAllValidationRuleElements().then((items) =>
            {
                var itemCount: number = 0;

                items.forEach(item =>
                {
                    itemCount += item.Children.length;
                });


                expect(itemCount).toBe(Number(num), itemCount + ' validation rules where found, count specifieds ' + num);

                ValidationRuleController.CloseValidatonRuleWindow().then(() => done());

            });

        });

    });

});

