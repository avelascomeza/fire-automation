/**
 * Validation Rule Spec
 * Runs a validation rule
 */

let validationRuleData = require("../data/validationRules");
let ValueController = require("../tsCompiled/ValueController").ValueController;
let jsonQuery = require('json-query');




let loadRuleData = function(ID)
{
    return jsonQuery('validationRules[ID='+ ID + ']', { data: validationRuleData }) ;

};

let createRuleTest = function(ID)
{
    var item = loadRuleData(ID).value;



    return () => {it("should verify the '" + item.Name + "' validation rule properly fires", done => {done();})};
};



describe("Validation Rule", createRuleTest("5"));


