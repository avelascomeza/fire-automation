import { FieldType } from '../Enums/enums';
import { PageNavigationController, Tab } from '../pagenavigationcontroller';
import { DataService } from '../Types/DataService';
import { ElementController } from '../Types/ElementController';
import { Field } from '../Types/Field';
import { FieldValue } from '../Types/fieldValue';
import { GenericErrorController } from '../Types/GenericErrorController';
import { HamburgerController } from '../Types/HamburgerController';
import { SpecialTabHelper } from '../Types/SpecialTabHelper';
import { TabValueClearer } from '../Types/TabValueClearer';
import { ValueSetter } from '../Types/valueSetter';
import { VeilController } from '../Types/VeilController';
import { element, browser } from 'protractor/built';
import { settings } from 'cluster';
import { by } from 'protractor';
import { error } from 'util';



describe("Print", function ()
{

    // it("Verifies the print can be canceled", done =>
    // {
    //     PageNavigationController.NavigateToTabReturnSuccess(Tab.basic).then(rs =>
    //     {
    //         HamburgerController.OpenAndCancelPrint().then(() =>
    //         {
    //             element(by.xpath(HamburgerController.NextButton)).isPresent().then(res =>
    //             {
    //                 expect(res).toBe(false, "Expected the print window to close but it did not");
    //                 done();
    //             });

    //         }).catch(err => { expect(err).toBe(null); done(); });
    //     });
    // });

    // it("Verifies error displayed when nothing is selected", done =>
    // {
    //     PageNavigationController.NavigateToTabReturnSuccess(Tab.basic).then(rs =>
    //     {
    //         HamburgerController.PrintNothing().then(() =>
    //         {

    //             Promise.all([GenericErrorController.IsErrorVisible(), GenericErrorController.GetTitleAndMessage()]).then(([visible, [title, msg]]) =>
    //             {
    //                 expect(visible).toBe(true, "error was not visible");
    //                 expect(title).toBe('Warning - Selections Required', "Text was incorrect, found: " + title);
    //                 expect(msg).toBe('At least one module must be selected to print a report.', "Text was incorrect, found: " + msg);
    //                 GenericErrorController.ClickOkButton().then(() =>
    //                 {
    //                     HamburgerController.ClickCancel().then(() => done());
    //                 });

    //             });


    //         }).catch(err => { expect(err).toBe(null); done(); });
    //     });
    // });



    // //fire-1689
    // it("Verifies the basic tab can be printed", done =>
    // {
    //     PageNavigationController.NavigateToTabReturnSuccess(Tab.basic).then(rs =>
    //     {
    //         HamburgerController.Print(["Basic"]).then(() =>
    //         {
    //             PrintSpecHelper.VerifyPrintView(done);



    //         });
    //     });
    // });

    // // //fire-1698
    // it("Verifies the units tab can be printed  (and non nfirs)", done =>
    // {
    //     PageNavigationController.NavigateToTabReturnSuccess(Tab.unitReports).then(rs =>
    //     {

    //         SpecialTabHelper.AddUnit(["2a234733-e8b6-4f5f-855a-582cfb7426c9"]).then(() =>
    //         {
    //             HamburgerController.Print(["Units/Resources", "Personnel", "Non-NFIRS Fields"]).then(() =>
    //             {

    //                 PrintSpecHelper.VerifyPrintView(done);

    //             }).catch(err => { expect(err).toBe(null); done(); });
    //         });
    //     });
    // });

    // // //fire-1698
    // it("Verifies that onces units are removed they cannot be printed (and non nfirs)", done =>
    // {
    //     PageNavigationController.NavigateToTabReturnSuccess(Tab.unitReports).then(rs =>
    //     {
    //         TabValueClearer.DeleteAllUnitsIgnoreMessage().then(() =>
    //         {

    //             HamburgerController.TryFindElementInPrint(["Units/Resources", "Personnel", "Non-NFIRS Fields"]).then(res =>
    //             {
    //                 expect(res).toBe(false, "found items for units with no units");
    //                 done();

    //             }).catch(err => { expect(err).toBe(null); done(); });
    //         });

    //     });
    // });


    //fire-1690
    it("Verifies the civilian casualties tab can be printed", done =>
    {

        SpecialTabHelper.EnableCasualties().then(() =>
        {
            PageNavigationController.NavigateToTabReturnSuccess(Tab.civilianCasualty).then(() =>
            {
                SpecialTabHelper.AddItemToTabItemCollection("Add Civilian").then(() =>
                {
                    HamburgerController.Print(['Civilian Casualty #1']).then(() =>
                    {
                        PrintSpecHelper.VerifyPrintView(done);
                    })

                }).catch(err => { expect(err).toBe(null); done(); });
            });
        });
    });

    it("Verifies the civilian casualties can be removed and not shown in print", done =>
    {

        TabValueClearer.DeleteAllCivilianCasualtiesIgnoreError().then(() =>
        {
            HamburgerController.TryFindElementInPrint(["Civilian Casualty #1"]).then(res =>
            {
                expect(res).toBe(false, "found items for civ casutalties with no civ casualties");
                done();

            }).catch(err => { expect(err).toBe(null); done(); });
        });
    });




    //fire-1691
    it("Verifies the fire casualties tab can be printed", done =>
    {

        SpecialTabHelper.EnableCasualties().then(() =>
        {
            PageNavigationController.NavigateToTabReturnSuccess(Tab.civilianCasualty).then(() =>
            {

                SpecialTabHelper.AddItemToTabItemCollection('Add Fire Service').then(() =>
                {
                    HamburgerController.Print(['Fire Casualty #1']).then(() =>
                    {
                        PrintSpecHelper.VerifyPrintView(done);
                    })

                });
            })

        })
    });

    it("Verifies the Fire Service casualties can be removed and not shown in print", done =>
    {
        TabValueClearer.DeleteAllFireCasualtiesIgnoreError().then(() =>
        {
            HamburgerController.TryFindElementInPrint(["Fire Service Casualty #1"]).then(res =>
            {
                expect(res).toBe(false, "found items for Fire Service casualties with no Fire Service casualties");
                done();

            }).catch(err => { expect(err).toBe(null); done(); });

        });
    });

    // // fire-1693
    // it("Verifies the ems patients tab can be printed", done =>
    // {

    //     SpecialTabHelper.EnableEMS().then(() =>
    //     {
    //         PageNavigationController.NavigateToTabReturnSuccess(Tab.ems).then(() =>
    //         {
    //             SpecialTabHelper.AddItemToTabItemCollection('Add Patient').then(() =>
    //             {

    //                 HamburgerController.Print(['Patient #1']).then(() =>
    //                 {
    //                     PrintSpecHelper.VerifyPrintView(done);
    //                 })
    //             })
    //         })

    //     })
    // });

    // it("Verifies the ems patients can be removed and not shown in print", done =>
    // {
    //     TabValueClearer.DeleteAllPatients().then(() =>
    //     {
    //         HamburgerController.TryFindElementInPrint(["Patient #1"]).then(res =>
    //         {
    //             expect(res).toBe(false, "found items for ems patients with no ems patients");
    //             done();

    //         }).catch(err => { expect(err).toBe(null); done(); });

    //     });
    // });



    // // fire-1695
    // it("Verifies the Hazmat tab can be printed", done =>
    // {

    //     SpecialTabHelper.EnableHazmat().then(() =>
    //     {
    //         PageNavigationController.NavigateToTabReturnSuccess(Tab.hazmat).then(() =>
    //         {
    //             DataService.Get('ItemName', '(4) - Released to county agency').then(res =>
    //             {
    //                 var val = res[0];

    //                 ValueSetter.SetField(new FieldValue(new Field("DISPOSITIONID", FieldType.SingleSelect, Tab.hazmat), val)).then(() =>
    //                 {

    //                     HamburgerController.Print(['Hazmat #1']).then(() =>
    //                     {
    //                         PrintSpecHelper.VerifyPrintView(done);
    //                     }).catch(err => { expect(err).toBe(null); done(); });
    //                 });

    //             });
    //         });
    //     });
    // });
    // it("Verifies the Hazmat tab can be  printed and shows multiple chemicals", done =>
    // {

    //     SpecialTabHelper.EnableHazmat().then(() =>
    //     {

    //         DataService.Get('ItemName', '1-Bromo-3-methylbutane').then(res =>
    //         {
    //             var chem1 = res[0];
    //             DataService.Get('ItemName', '1-Decene').then(secnd =>
    //             {
    //                 var chem2 = secnd[0];


    //                 var val1 = new FieldValue(new Field("CHEMICALID", FieldType.SingleSelect, Tab.hazmat), chem1);
    //                 var val2 = new FieldValue(new Field("CHEMICALID", FieldType.SingleSelect, Tab.hazmat), chem2);


    //                 ValueSetter.SetField(new FieldValue(new Field("CHEMICALCOLLECTION", FieldType.CollectionWithData, Tab.hazmat), [[val1], [val2]])).then(() =>
    //                 {

    //                     HamburgerController.Print(['Hazmat #1', 'Hazmat #2']).then(() =>
    //                     {
    //                         PrintSpecHelper.VerifyPrintView(done);
    //                     }).catch(err => { expect(err).toBe(null); done(); });
    //                 });
    //             });
    //         });
    //     });
    // });
    // it("Verifies the hazmat can be removed and not shown in print", done =>
    // {
    //     TabValueClearer.ClearTabValues(Tab.hazmat).then(() =>
    //     {
    //         HamburgerController.TryFindElementInPrint(["Hazmat #1"]).then(res =>
    //         {
    //             expect(res).toBe(false, "found items for hazmat with no hazmat data");
    //             done();

    //         }).catch(err =>
    //         {
    //             fail(err);

    //         });
    //     });
    // });



    // // fire-1697
    // it("Verifies the wildland tab can be printed", done =>
    // {
    //     SpecialTabHelper.EnableWildland().then(() =>
    //     {
    //         PageNavigationController.NavigateToTabReturnSuccess(Tab.wildland).then(() =>
    //         {
    //             DataService.Get('ItemName', '(1) - Rural, including farms > 50 acres').then(res =>
    //             {
    //                 var val = res[0];

    //                 ValueSetter.SetField(new FieldValue(new Field("AREATYPEID", FieldType.SingleSelect, Tab.wildland), val)).then(() =>
    //                 {

    //                     HamburgerController.Print(['Wildland']).then(() =>
    //                     {
    //                         PrintSpecHelper.VerifyPrintView(done);
    //                     }).catch(err => { expect(err).toBe(null); done(); });

    //                 })

    //             })
    //         })
    //     })

    // });

    // it("Verifies when wildland is cleared it cannot be printed", done =>
    // {
    //     TabValueClearer.ClearTabValues(Tab.wildland).then(() =>
    //     {
    //         HamburgerController.TryFindElementInPrint(["Wildland"]).then(res =>
    //         {
    //             expect(res).toBe(false, "found items for wildland with no wildland data");
    //             done();

    //         }).catch(err => { expect(err).toBe(null); done(); });

    //     });
    // });


    // //fire-1701
    // it("Verifies the fire tab can be printed [Any exposure that has any data in fire fields other than structure/protective systems should have a fire form printable.]", done =>
    // {
    //     SpecialTabHelper.EnableFire().then(() =>
    //     {
    //         PageNavigationController.NavigateToTabReturnSuccess(Tab.fire).then(() =>
    //         {
    //             ValueSetter.SetField(new FieldValue(new Field("FIREALARMS", FieldType.String, Tab.fire), "2")).then(() =>
    //             {

    //                 HamburgerController.Print(['Fire']).then(() =>
    //                 {
    //                     PrintSpecHelper.VerifyPrintView(done);
    //                 }).catch(err => { expect(err).toBe(null); done(); });

    //             })


    //         });
    //     })

    // });

    // it("Verifies the fire data can be cleared and not shown in print", done =>
    // {
    //     TabValueClearer.ClearTabValues(Tab.fire).then(() =>
    //     {
    //         HamburgerController.TryFindElementInPrint(["Fire"]).then(res =>
    //         {
    //             expect(res).toBe(false, "found items for fire with no fire data");
    //             done();

    //         }).catch(err => { expect(err).toBe(null); done(); });

    //     });
    // });


    // //fire-1702
    // it("Verifies the structure can be printed [Any exposure that has fire structure or fire protective systems information filled out should have a form available to print.]", done =>
    // {
    //     SpecialTabHelper.EnableStructureFire().then(() =>
    //     {
    //         PageNavigationController.NavigateToTabReturnSuccess(Tab.fire).then(() =>
    //         {

    //             DataService.Get('ItemName', '(7) - Underground structure work area').then(res =>
    //             {
    //                 var val = res[0];

    //                 ValueSetter.SetField(new FieldValue(new Field("STRUCTURETYPEID", FieldType.SingleSelect, Tab.fire), val)).then(() =>
    //                 {



    //                     HamburgerController.Print(['Structure Fire']).then(() =>
    //                     {
    //                         PrintSpecHelper.VerifyPrintView(done);
    //                     }).catch(err => { expect(err).toBe(null); done(); });

    //                 });
    //             });


    //         });
    //     })

    // });

    // it("Verifies the structre fire can be removed and not shown in print", done =>
    // {
    //     TabValueClearer.ClearTabValues(Tab.fire).then(() =>
    //     {
    //         HamburgerController.TryFindElementInPrint(["Structure Fire"]).then(res =>
    //         {
    //             expect(res).toBe(false, "found items for Structure Fire with no Structure Fire data");
    //             done();

    //         }).catch(err =>
    //         {
    //             fail(err);

    //         });

    //     });
    // });



    // // //fire-1703
    // it("Verifies the arson can be printed [Any exposure that has any arson information filled out should have a form available to print.]", done =>
    // {
    //     SpecialTabHelper.EnableArson().then(() =>
    //     {

    //         DataService.Get('ItemName', 'E-only').then(res =>
    //         {
    //             var val = res[0];

    //             ValueSetter.SetField(new FieldValue(new Field("ARSONAGENCYID", FieldType.SingleSelect, Tab.arson), val)).then(() =>
    //             {
    //                 HamburgerController.Print(['Arson']).then(() =>
    //                 {
    //                     PrintSpecHelper.VerifyPrintView(done);
    //                 }).catch(err => { expect(err).toBe(null); done(); });

    //             });
    //         });
    //     });

    // });

    // it("Verifies the arson data can be cleared and not shown in print", done =>
    // {
    //     TabValueClearer.ClearTabValues(Tab.arson).then(() =>
    //     {
    //         HamburgerController.TryFindElementInPrint(["Arson"]).then(res =>
    //         {
    //             expect(res).toBe(false, "found items for arson with no arson data");
    //             done();

    //         }).catch(err =>
    //         {
    //             fail(err);

    //         });

    //     });
    // });


/* 
    // //fire-1704
    it("Verifies the juevenile firesetters can be printed [Any exposure with juvenile firesetters added to the arson module should have a form available to print for each firesetter, IF remarks has been filled out on the main arson page.]", done =>
    {
        SpecialTabHelper.EnableArson().then(() =>
        {
            DataService.Get('ItemName', '(1) - Male').then(res =>
            {
                var val = res[0];
                var genderFieldVal = new FieldValue(new Field('ARSONJUVENILEFIRESETTERGENDERID', FieldType.SingleSelect, Tab.arson), val);

                ValueSetter.SetField(new FieldValue(new Field("ARSONJUVENILEFIRESETTERCOMPLEXCOLLECTION", FieldType.CollectionWithData, Tab.arson), [[genderFieldVal]])).then(() =>
                {
                    //make sure remarks is null
                    ValueSetter.SetField(new FieldValue(new Field("ARSONREMARKS", FieldType.String, Tab.arson), null)).then(() =>
                    {
                        HamburgerController.TryFindElementInPrint(["undefined #1"]).then(res =>
                        {
                            expect(res).toBe(false, "found items for arson juevenile though remarks where null");
                            done();

                        });
                    });

                });
            });
        });

    });

    it("Verifies the juevenile firesetters can be prrinted when has remarks", done =>
    {
        SpecialTabHelper.EnableArson().then(() =>
        {
            DataService.Get('ItemName', '(1) - Male').then(res =>
            {
                var val = res[0];
                var genderFieldVal = new FieldValue(new Field('ARSONJUVENILEFIRESETTERGENDERID', FieldType.SingleSelect, Tab.arson), val);

                ValueSetter.SetField(new FieldValue(new Field("ARSONJUVENILEFIRESETTERCOMPLEXCOLLECTION", FieldType.CollectionWithData, Tab.arson, ), [[genderFieldVal]])).then(() =>
                {
                    //make sure remarks is null
                    ValueSetter.SetField(new FieldValue(new Field("ARSONREMARKS", FieldType.String, Tab.arson), "Call me Ishmeal...")).then(() =>
                    {
                        HamburgerController.Print(['Juvenile Firesetter #1']).then(() =>
                        {
                            PrintSpecHelper.VerifyPrintView(done);
                        });


                    }).catch(err => { expect(err).toBe(null); done(); });
                });
            });
        });

    });


    it("Verifies the arson juevenile can be removed and not shown in print", done =>
    {
        TabValueClearer.ClearTabValues(Tab.arson).then(() =>
        {
            HamburgerController.TryFindElementInPrint(["Juvenile Firesetter #1"]).then(res =>
            {
                expect(res).toBe(false, "found items for ems patients with no ems patients");
                done();

            }).catch(err => { expect(err).toBe(null); done(); });

        });
    });


    //fire-1705
    //Owner is involved, and number of people involved in the incident > 0
    //Owner is not involved, and number of people involved in the incident > 1
    //The size of all the combined remarks from basic and units exceeds 21 lines of 84 characters
    it("Verifies the supplemental remarks can be printed [Owner is involved, and number of people involved in the incident > 0]", done =>
    {
        DataService.Get('ItemName', '(MR) - Mr.').then(res =>
        {
            var val = res[0];
            var titleFieldVal = new FieldValue(new Field('PERSONNAMEPREFIXID', FieldType.SingleSelect, Tab.basic), val);
            var nameFieldVal = new FieldValue(new Field('PERSONFIRSTNAME', FieldType.String, Tab.basic), 'BOB');
            ValueSetter.SetField(new FieldValue(new Field("PERSONCOLLECTION", FieldType.CollectionWithData, Tab.basic), [[titleFieldVal, nameFieldVal]])).then(() =>
            {
                SpecialTabHelper.AddOwner(true).then(() =>
                {
                    HamburgerController.Print(['Supplemental']).then(() =>
                    {
                        PrintSpecHelper.VerifyPrintView(done);
                    }).catch(err => { expect(err).toBe(null); done(); });
                });
            });
        });
    });

    it("Verifies the supplemental remarks can be printed [Owner is not involved, and number of people involved in the incident > 1]", done =>
    {
        SpecialTabHelper.EditOwner(false).then(() =>
        {
            HamburgerController.TryFindElementInPrint(["Supplemental"]).then(res =>
            {
                expect(res).toBe(false, "found items for Supplemental patients with no owner and one person");
                done();
            }).catch(err => { expect(err).toBe(null); done(); });
        });
    });

    it("Verifies the supplemental remarks can be printed [Owner is not involved, and number of people involved in the incident > 1 (add person)]", done =>
    {
        DataService.Get('ItemName', '(MR) - Mr.').then(res =>
        {
            var val = res[0];
            var titleFieldVal = new FieldValue(new Field('PERSONNAMEPREFIXID', FieldType.SingleSelect, Tab.basic), val);
            var nameFieldVal = new FieldValue(new Field('PERSONFIRSTNAME', FieldType.String, Tab.basic), 'BOB');


            var title2FieldVal = new FieldValue(new Field('PERSONNAMEPREFIXID', FieldType.SingleSelect, Tab.basic), val);
            var name2FieldVal = new FieldValue(new Field('PERSONFIRSTNAME', FieldType.String, Tab.basic), 'JORGE');

            ValueSetter.SetField(new FieldValue(new Field("PERSONCOLLECTION", FieldType.CollectionWithData, Tab.basic), [[titleFieldVal, nameFieldVal], [title2FieldVal, name2FieldVal]])).then(() =>
            {
                SpecialTabHelper.AddOwner(false).then(() =>
                {
                    HamburgerController.Print(['Supplemental']).then(() =>
                    {
                        PrintSpecHelper.VerifyPrintView(done);
                    }).catch(err => { expect(err).toBe(null); done(); });
                });
            });
        });
    });

    it("Verifies the supplemental remarks can be printed [The size of all the combined remarks from basic and units exceeds 21 lines of 84 characters]", done =>
    {
        ValueSetter.SetField(new FieldValue(new Field("PERSONCOLLECTION", FieldType.CollectionWithData, Tab.basic), null)).then(() =>
        {
            var remarks = new FieldValue(new Field("NARRATIVEREMARKS",
                FieldType.String, Tab.basic), "What is Lorem Ipsum?Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Why do we use it?It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).Where does it come from?Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.");

            ValueSetter.SetField(new FieldValue(new Field("NARRATIVECOLLECTION", FieldType.CollectionWithData, Tab.basic), [[remarks]])).then(() =>
            {
                HamburgerController.Print(['Supplemental']).then(() =>
                {
                    PrintSpecHelper.VerifyPrintView(done);
                }).catch(err => { expect(err).toBe(null); done(); });

            });
        });
    }, 100000); */
});


class PrintSpecHelper
{
    public static VerifyPrintView(done)
    {
        ElementController.IsPresent("//print-view").then(vs =>
        {
            expect(vs).toBe(true, "Print View was not displayed");

            HamburgerController.ClosePrintView().then(() =>
            {
                ElementController.IsPresent("//print-view").then(closedView =>
                {
                    expect(closedView).toBe(false, "Print View was still displayed after clicking ok");
                    done();
                });
            });

        }).catch(err => { expect(err).toBe(null); done(); });

    }
}

