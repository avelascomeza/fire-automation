import { NFIRSInitializer } from '../nfirsinitializer';
import { ElementController } from '../Types/ElementController';
import * as console from 'console';
import { FieldValue } from '../Types/FieldValue';
import * as assert from 'assert';
import { $, browser } from 'protractor/built';
import { error, isNullOrUndefined, print } from 'util';

import { _ } from 'lodash';

import { TestExecutionRule } from '../Types/TestExecutionRule';
import { ValueController } from '../Types/ValueController';
import { ValueSetter } from '../Types/ValueSetter';
import { FieldType } from '../Enums/enums';
import { Field } from '../Types/Field';
import { ScreenshotReporter } from 'protractor-beautiful-reporter'

//let testExecutions: Array<TestExecutionRule> = require("../../data/BusinessRules_Destructives");
let testExecutions: Array<TestExecutionRule> = require("../../data/IndivTest");
//let testExecutions: Array<TestExecutionRule> = require("../../data/Bugs");
//let testExecutions: Array<TestExecutionRule> = require("../../data/FieldConstraints");

interface Array<T>
{
    groupBy(obj: Object): any;
}
Object.defineProperty(Array.prototype, 'groupBy', {
    value: function (prop)
    {
        return this.reduce(function (groups, item)
        {
            var val = item[prop];
            groups[val] = groups[val] || [];
            groups[val].push(item);
            return groups;
        }, {});
    }
});

export class Runner
{
    public static async RunAll()
    {
        //group the tests
        let groupByJiraCode = testExecutions.groupBy("Jira");

        for (let key of Object.keys(groupByJiraCode))
        {
            let group = groupByJiraCode[key];

            var first: TestExecutionRule = group[0];

            var describeString = "[" + first.Jira + "] ";

            if (first.Type == "FieldConstraint")
            {
                describeString += "[" + first.Name + "] => [" + first.RuleDescription + "]";
            }
            else
            {
                describeString += "Verifies that [" + first.Name + "] when [" + first.RuleDescription + "]";
            }

            describe(describeString, function ()
            {
                let i = 1;
                for (let rule of group)
                {
                    Runner.ItRunner(rule, "[" + rule.Jira + "] Verify [" + i + "]");
                    i++;


                }
            });

        }
    }


    public static async ItRunner(rule: TestExecutionRule, description: string)
    {
        return it(description, async f =>
        {
            var HtmlReporter = require("protractor-beautiful-reporter");

            HtmlReporter.AddTestData("HELLO");
            try
            {
                ValueController.CurrentFieldValues = [];
                browser.executeScript("console.info('SETUP:')");
                // browser.executeScript("console.info('" + JSON.stringify(rule.Setup, undefined, 4) + "')");
                for (var fv of rule.Setup)
                {
                    await ValueSetter.SetField(fv, false, true);
                }

                browser.executeScript("console.info('ACT:')");
                // browser.executeScript("console.info('" + JSON.stringify(rule.Act, undefined, 4) + "')");
                for (var fv of rule.Act)
                {
                    await ValueSetter.SetField(fv, false, true);
                }


                if (rule.RequiresRefresh)
                {
                    await browser.driver.navigate().refresh();
                }

                browser.executeScript("console.info('ASSERT:')");
                //  browser.executeScript("console.info('" + JSON.stringify(rule.Assert, undefined, 4) + "')");
                for (var assertVal of rule.Assert)
                {
                    let currentVal: FieldValue = null;
                    try
                    {
                        currentVal = await ValueController.GetFieldValue(assertVal.Field, assertVal.Value);
                    }
                    catch (err)
                    {
                        if (assertVal.Value != null)
                        {
                            expect(err.toString() + "\n").toBeUndefined("\nAn error occured in retrieving value for" + JSON.stringify(assertVal.Field, undefined, 4));
                        }
                    }

                    var matchResult = await this.VerifyCurrentMatchesExpected(currentVal, assertVal);

                    expect(matchResult.FAILDATA).toBeNull();

                }
            }
            catch (error)
            {
                expect(error.toString() + "\n").toBeUndefined("\nAn unhandled exception has failed the test");
            }

            f();
        }, 1000000);
    };

    private static async VerifyCurrentMatchesExpected(current: FieldValue, assert: FieldValue): Promise<{ DiditPass: boolean, FAILDATA: any }>
    {

        var res = { DiditPass: false, FAILDATA: null };
        if (current == null)
        {
            res.DiditPass = assert.Value == null;

            if (!res.DiditPass)
            {
                res.FAILDATA = { Actual: null, Expected: assert.Value, Field: assert.Field };
            }
            return res;
        }
        else if (assert.Field.DataTypeID == FieldType.CollectionWithData)
        {
            return await this.VerifyComplexValues(current, assert)
        }
        else
        {
            res.DiditPass = JSON.stringify(current.Value, undefined, 4) == JSON.stringify(current.Value, undefined, 4);

            if (!res.DiditPass)
            {
                res.FAILDATA = { Actual: current.Value, Expected: assert.Value, Field: assert.Field };
            }
            return res;
        }
    }

    private static async VerifyComplexValues(current: FieldValue, assert: FieldValue): Promise<{ DiditPass: boolean, FAILDATA: any }>
    {
        var failData = null;
        for (var wantedCollection of assert.Value as FieldValue[][])
        {
            for (var wantedValue of wantedCollection)
            {
                var matchingCurrentValues = [];

                var found = false;
                for (let curValCollection of current.Value as FieldValue[][])
                {
                    if (found) break;

                    for (let curentFieldVal of curValCollection)
                    {
                        if (curentFieldVal.Field.FieldRef == wantedValue.Field.FieldRef)
                        {
                            var matchResult = await this.VerifyCurrentMatchesExpected(curentFieldVal, wantedValue);

                            if(matchResult.DiditPass)
                            {
                                found = true;
                                break;
                            }
                            else
                            {
                                matchingCurrentValues.push(curentFieldVal.Value);
                            }
                        }
                    }
                }

                if (!found)
                {
                    if (failData == null)
                    {
                        failData = [];
                    }

                    failData.push({ Actual: matchingCurrentValues, Expected: wantedValue.Value, Field: wantedValue.Field });

                }

            }

        }


        return { DiditPass: (failData == null), FAILDATA: failData };
    }

}


describe("Destructive Rule Tests", function ()
{

    beforeEach(NFIRSInitializer.CreateNewIncident);

    Runner.RunAll();


});


