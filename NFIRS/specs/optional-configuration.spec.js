/**
 * OPTIONAL CONFIGURATIONS
 * EHR > Optional Configurations
 */
let pageName = "OPTIONAL CONFIGURATIONS";
let pageUrl = "#/sitenode/4-51";
let pageDirective = "optional-configurations";
let adminHeading = require("../page-objects/adminHeading");
let functionBar = require("../page-objects/functionBar");
let itemList = require("../page-objects/itemList");
let sortDataModal = require("../page-objects/sortDataModal");
let errorModal = require("../page-objects/errorModal");
let randomHelpers = require("../../helpers/random-helpers");
let functionBarSharedTests = require("../shared-specs/function-bar.spec");

describe(pageName, () => {

                // Check the Page Header behavior.
    describe("Header Section", () => {

        // Re load the page.
        beforeAll(done => {
            EsoInitializer.loadPage(pageUrl, pageDirective)
            .then(() => { return errorModal.errorsExist(); })
            .then(errorsExist => {
                // Expect that there are no errors displayed.
                expect(errorsExist).toBe(false, `Error loading page: ${pageUrl} with directive: ${pageDirective}.`);
                // If there are, we need to clear them for continued testing.
                if (errorsExist) { errorModal.clearErrorModal().then(done) }
                else { done(); }
            });
        });

        it(`has a page Title of '${pageName}'`, done => {
            adminHeading.getTitle().then(title => {
                expect(title).toMatch(pageName);
                done();
            });
        });

    });

    describe("Item List", () => {

        // Re load the page.
        beforeAll(done => {
            EsoInitializer.loadPage(pageUrl, pageDirective)
            .then(() => { return errorModal.errorsExist(); })
            .then(errorsExist => {
                // Expect that there are no errors displayed.
                expect(errorsExist).toBe(false, `Error loading page: ${pageUrl} with directive: ${pageDirective}.`);
                // If there are, we need to clear them for continued testing.
                if (errorsExist) { errorModal.clearErrorModal().then(done) }
                else { done(); }
            });
        });

        it("Toggles an item without error.", done => {
            itemList.toggleRow(0)
            .then(() => { return errorModal.errorsExist(); })
            .then(errorsExist => {
                // Expect that there are no errors displayed.
                expect(errorsExist).toBe(false, `Error modal exists on page after test.`);
                // If there are, we need to clear them for continued testing.
                if (errorsExist) { errorModal.clearErrorModal().then(done) }
                else { done(); }
            });
        });

        it("Toggles the item back without error.", done => {
            itemList.toggleRow(0)
            .then(() => { return errorModal.errorsExist(); })
            .then(errorsExist => {
                // Expect that there are no errors displayed.
                expect(errorsExist).toBe(false, `Error modal exists on page after test.`);
                // If there are, we need to clear them for continued testing.
                if (errorsExist) { errorModal.clearErrorModal().then(done) }
                else { done(); }
            });
        });

    });

});