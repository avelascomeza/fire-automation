import { elementAt } from 'rxjs/operator/elementAt';
import { resolve } from 'path';
import { browser, Browser, by, element } from 'protractor/built';
import { ElementFinder } from 'protractor/built/element';
import { isNullOrUndefined, isNumber } from 'util';
import { FieldType } from '../Enums/enums';
import { Field } from './Field';
import { PageNavigationController, Tab } from '../pagenavigationcontroller';
import { FieldsetCompareResult, FieldValue, FieldValueCompare } from './FieldValue';
import { DataService } from './DataService';
import * as console from 'console'; import { ElementController } from './ElementController';




export class ValueController
{

    static CurrentFieldValues: FieldValue[] = [];

    static async LoadInitialValues()
    {
        await browser.waitForAngular();
        for (let k in Tab)
        {
            if (k == "unkown")
            {
                continue;
            }
            if (!isNaN(Number(k)))
            {
                if (await PageNavigationController.NavigateToTabReturnSuccess(Number(k)))
                {
                    //we are on the basic page so load
                    await this.LoadAllValuesOnPage();
                }
            }

        }
        console.log(JSON.stringify(this.CurrentFieldValues));
        return true;
    }

    static async LoadAllValuesOnPage()
    {

        let foundValues = [];
        await browser.waitForAngular();

        let promises = [
            this.FindCtrlsAndGetValues('//eso-control/eso-text/input[@type="text"]', 'ancestor::eso-text', FieldType.String),
            this.FindCtrlsAndGetValues('//eso-control/eso-date/*/*/div[@class="display-value"]', 'ancestor::eso-date', FieldType.DateTime),
            this.FindCtrlsAndGetValues('//eso-control/text-time/eso-masked-input/input', 'ancestor::text-time', FieldType.Time),
            this.FindCtrlsAndGetValues('//eso-yes-no/button', 'ancestor::eso-yes-no', FieldType.Boolean),
            this.FindCtrlsAndGetValues("//eso-control/eso-number-input/input[@type='number']", 'ancestor::eso-number-input', FieldType.Number)

        ];

        let esoFields = element.all(by.xpath("//eso-field"));

        let promRes = await Promise.all(promises);

        for (let prom of promRes)
        {
            for (let fieldVal of prom)
            {
                if (!isNullOrUndefined(fieldVal.Field.FieldRef))
                {
                    foundValues.push(fieldVal);
                }

            }

        }


        var esoFieldRs: ElementFinder[] = await esoFields;
        var labelsAndRequireds = [];
        for (var fld of esoFieldRs)
        {
            labelsAndRequireds.push(this.GetAndSetFieldLabel(foundValues, fld));


        }

        await Promise.all(labelsAndRequireds);


        return foundValues;

    };

    private static async GetAndSetFieldLabel(foundValues, fld: ElementFinder)
    {
        var label = await fld.getAttribute('field-label');

        var ref = await fld.element(by.xpath('./eso-control/*[@field-label="' + label + '"]'));

        var refVal = await ref.getAttribute('field-ref');




        var match: FieldValue = foundValues.find(fv => fv.Field.FieldRef == refVal);

        if (!isNullOrUndefined(match))
        {
            match.Field.FieldLabel = label;
            match.Field.IsRequired = await browser.isElementPresent(by.xpath('.//div[text()="Required"]'));

        };


    }




    private static async FindCtrlsAndGetValues(xpathString: string, ancestorString: string, fldType: FieldType): Promise<FieldValue[]>
    {
        var elements = await element.all(by.xpath(xpathString));

        return this.GetAllControlValues(elements, ancestorString, fldType);

    }

    private static async FindCtrlAndGetValues(xpathString: string, ancestorString: string, fldType: FieldType): Promise<FieldValue>
    {

        var elem = await ElementController.WaitForClickable(xpathString);

        var vals = await this.GetControlValues(elem, ancestorString, fldType);




        return vals;

    }


    private static GetAllControlValues(ctrls: ElementFinder[], ancestorString: string, fldType: FieldType): Promise<FieldValue[]>
    {
        let promises = [];
        for (let ctrl of ctrls)
        {
            promises.push(this.GetControlValues(ctrl, ancestorString, fldType));
        }

        return Promise.all(promises);

    }

    private static async GetControlValues(ctrl: ElementFinder, ancestorString: string, fldType: FieldType): Promise<FieldValue>
    {


        var value = await ctrl.getAttribute('value');

        if (isNullOrUndefined(value))
        {
            value = await ctrl.getText();
        }

        var ref = ctrl.element(by.xpath(ancestorString)).getAttribute('field-ref');
        var enabled = ctrl.isEnabled();
        if (value == '' || value == 'hh:mm:ss' || value == "mm/dd/yyyy")
        {
            value = null;
        }

        if ((fldType == FieldType.SingleSelect || fldType == FieldType.NestedSingleSelect) && value != null)
        {
            value = (await DataService.Get('ListFieldValues', 'ItemName', value))[0];
        }

        var fieldval = new FieldValue(new Field(await ref, fldType, PageNavigationController.CurrentPage), value);
        fieldval.Field.IsEnabled = await enabled;


        return fieldval;
    }




    public static CompareValueSets(baseValue: FieldValue[], compareValue: FieldValue[], fieldRefsToSkip: string[], typesToSkip: FieldType[], ignoreRequired: boolean, ignoreEnabled: boolean): FieldsetCompareResult
    {

        var missingValues: FieldValue[] = [];
        var changedValues: FieldValueCompare[] = [];

        baseValue.forEach(originalValue =>
        {
            var foundVal: FieldValue = compareValue.find(c => c.Field.FieldRef === originalValue.Field.FieldRef);

            if (isNullOrUndefined(foundVal))
            {
                missingValues.push(originalValue);
            }
            else if (foundVal.Value !== originalValue.Value || (!ignoreRequired && foundVal.Field.IsRequired !== originalValue.Field.IsRequired) || (!ignoreEnabled && foundVal.Field.IsEnabled !== originalValue.Field.IsEnabled))
            {
                if (!typesToSkip.some(typ => typ == foundVal.Field.DataTypeID) && !fieldRefsToSkip.some(fr => fr == foundVal.Field.FieldRef))
                {
                    changedValues.push(new FieldValueCompare(originalValue, foundVal));
                }
            }

        });

        var newValues: FieldValue[] = [];

        compareValue.forEach(comp =>
        {

            if (!baseValue.some(b => b.Field.FieldRef == comp.Field.FieldRef))
            {
                newValues.push(comp);
            }


        })


        return new FieldsetCompareResult(missingValues, changedValues, newValues);





    };


    public static async GetFieldValue(field: Field, value: any, needsNavigation: boolean = true)
    {
        if (needsNavigation)
        {
            await PageNavigationController.NavigateToTabReturnSuccess(field.TabID);
        }

        if (field.DataTypeID == FieldType.CollectionWithData)
        {


            return await this.GetFieldValueForComplex(field, (value as any[]));

        }
        else
        {
            let controlString = '';
            let ancestorString = '';


            switch (field.DataTypeID)
            {
                case FieldType.Boolean:
                    controlString = '//eso-yes-no/button';
                    ancestorString = 'ancestor::eso-yes-no';
                    break;
                case FieldType.DateTime:
                    controlString = '//eso-control/eso-date[@field-ref="' + field.FieldRef + '"]//div[contains(@class,"display-value")]';
                    ancestorString = 'ancestor::eso-date';
                    break;
                case FieldType.String:
                    if (field.IsVirtual)
                    {
                        controlString = '//eso-control/eso-text[contains(@field-label,"' + field.DisplayName + '")]/input[@type="text"]';
                    }
                    else
                    {
                        controlString = '//eso-text[@field-ref="' + field.FieldRef + '"]/*[@type="text"]';
                    }


                    ancestorString = 'ancestor::eso-text';
                    break;

                case FieldType.Integer:
                case FieldType.Number:
                    controlString = '//eso-control/eso-number-input[@field-ref="' + field.FieldRef + '"]/input[@type="number"]';
                    ancestorString = 'ancestor::eso-number-input';
                    break;

                case FieldType.NestedSingleSelect:
                case FieldType.SingleSelect:
                    controlString = '//eso-control/eso-single-select[@field-ref="' + field.FieldRef + '"]//div[contains(@class,"display-value")]';
                    ancestorString = 'ancestor::eso-single-select';
                    break;

                //case FieldType.CollectionWithData:
                //case FieldType.Integer:
                default:
                    throw "UNKNOWN";

            }


            var val = await this.FindCtrlAndGetValues(controlString, ancestorString, field.DataTypeID);


            return val;
        }

    };

    private static async GetFieldValueForComplex(field: Field, value: any[]): Promise<FieldValue>
    {

        let fieldsToFind: FieldValue[];
        //find the different specific fields to look for
        if (value != null)
        {
            fieldsToFind = [];
            for (let valArray of value)
            {
                for (let fv of valArray)
                {
                    if (!fieldsToFind.some(f => f.Field.FieldRef == fv.Field.FieldRef))
                    {
                        fieldsToFind.push(fv);
                    }

                }
            }
        }


        var allgridRows: ElementFinder[] = null;
        if (field.ID.startsWith("casualty") && field.ID.endsWith("Casualties"))
        {
            var headerString = "casualty";
            var enderString = "casualties";

            var type = field.ID.substring(headerString.length + 1);
            type = type.substring(0, type.length - enderString.length);

            allgridRows = await ElementController.GetAllElements("//grid-row[contains(@ng-repeat,'" + headerString + "')][grid-cell[tag-text[contains(@ng-if,'" + type + "')]]]");
        }
        else if (field.ID.startsWith("unit"))
        {
            var headerString = "unitReports";

            allgridRows = await ElementController.GetAllElements("//grid-row[contains(@ng-repeat,'" + headerString + "')]");
        }
        else if (field.ID.startsWith("basic.location.") || field.ID.startsWith("arson.agencyReferredTo.address"))
        {
            allgridRows = await ElementController.GetAllElements('//eso-address-summary[contains(@field-label,"' + field.DisplayName + '")]');
        }
        else if (field.ID.startsWith("basic.personEntity.owner"))
        {
            var rowsXpath = "//custom-complex-collection[@field-ref='PERSONCOLLECTION']//grid-row[contains(@ng-if,'ownerHasValue')]";
            allgridRows = await ElementController.GetAllElements(rowsXpath);
        }
        else
        {
            var rowsXpath = "//custom-complex-collection[@field-ref='" + field.FieldRef + "']//grid-row[contains(@ng-repeat,'item')]";
            var allgridRows = await ElementController.GetAllElements(rowsXpath);
        }


        let vals: FieldValue[][] = [];

        if (allgridRows.length == 0)
        {
            vals == null;
        }
        else
        {

            for (var row of allgridRows)
            {
                await row.click();
                browser.waitForAngular();

                var hasShelf = await ElementController.IsPresent("//shelf-panel");

                if (fieldsToFind == null || fieldsToFind.length == 0)
                {
                    if (hasShelf) 
                    {
                        vals.push(await this.GetAllValuesInShelf())
                    }
                    else
                    {
                        vals.push(await this.LoadAllValuesOnPage());
                    }
                }
                else
                {
                    var valArray: FieldValue[] = [];
                    for (let fv of fieldsToFind)
                    {
                        valArray.push(await this.GetFieldValue(fv.Field, fv.Value, false));
                    }

                    vals.push(valArray);
                }

                if (hasShelf)
                {
                    await ElementController.WaitForClickableAndClick("//shelf-panel//button[text()='OK']");
                }
                else
                {
                    //is this handled?
                    await ElementController.WaitForClickableAndClick("//main-viewport/header//button[text()='OK']");
                    
                }
            }

        }

        return new FieldValue(field,vals);

    };



    public static async GetAllValuesInShelf()
    {
        var esoControlInShelf = "//shelf-panel//eso-field//eso-control/*[1]";

        var allEsoControls = await ElementController.GetAllElements(esoControlInShelf);

        let values = [];

        for (var control of allEsoControls)
        {
            var ref = await control.getAttribute("field-ref");


            if (!isNullOrUndefined(ref))
            {
                var field = (await DataService.Get("Fields", "FieldRef", ref))[0];

                if (!isNullOrUndefined(field))
                {
                    try
                    {
                        var value = await this.GetFieldValue(field, null, false);

                        values.push(value);
                    }
                    catch (err)
                    {

                    }
                }
            }
            else
            {
                var label = await control.getAttribute("field-label");


                var result = await DataService.Get("Fields", "DisplayName", label.split("'").join(""));
                var field = result[0];

                if (!isNullOrUndefined(field))
                {
                    try
                    {
                        var value = await this.GetFieldValue(field, null, false);

                        values.push(value);
                    }
                    catch (err)
                    {

                    }
                }
            }

        }

        return values;


    }


};


