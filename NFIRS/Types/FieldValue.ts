import { FieldType } from '../Enums/enums';
import { Tab } from '../pagenavigationcontroller';
import { isNullOrUndefined } from 'util';
import {Field} from './Field';

export class FieldValue
{
    public Field: Field;

    Value: any;

    constructor(field: Field, value: any)
    {
        this.Field = field;
        this.Value = value;
    };



};

export class FieldValueCompare
{
    ExpectedValue: FieldValue;
    ActualValue: FieldValue;

    constructor(expected: FieldValue, actual: FieldValue)
    {
        this.ExpectedValue = expected;
        this.ActualValue = actual;
    }
};


export class FieldsetCompareResult
{
    MissingValues: FieldValue[];
    ChangedValues: FieldValueCompare[];
    NewValues: FieldValue[];


    constructor(missingValues: FieldValue[], changedValues: FieldValueCompare[], newValues: FieldValue[])
    {
        this.MissingValues = missingValues;
        this.ChangedValues = changedValues;
        this.NewValues = newValues;
    }

}