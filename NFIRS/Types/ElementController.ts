import { element, browser } from 'protractor';
import { by, ExpectedConditions } from 'protractor/built';
import { ElementFinder } from 'protractor/built/element';
export class ElementController
{


    public static async GetElement(xpath: string): Promise<ElementFinder>
    {

        return await element(by.xpath(xpath));
    }

    public static async GetIfPresent(xpath: string): Promise<ElementFinder>
    {
        var element = await ElementController.GetElement(xpath);

        if (await element.isPresent())
        {
            return element;
        }
        else
        {
            return null;
        }


    }
    public static async GetAllElements(xpath: string): Promise<ElementFinder[]>
    {

        return await element.all(by.xpath(xpath));
    }
    public static async IsPresent(xpath: string): Promise<boolean>
    {
        var ele: ElementFinder = await element(by.xpath(xpath));

        return await ele.isPresent();
    }

    public static GetElementText(xpath: string): Promise<string>
    {
        return this.GetElementAttribute(xpath, 'value');
    }

    public static async GetTextElementText(xpath: string): Promise<string>
    {
        var element: ElementFinder = await this.GetElement(xpath);
        return await element.getText();
    }

    public static async ClickElement(xpath: string): Promise<void>
    {
        var ele: ElementFinder = await element(by.xpath(xpath));

        await ele.click();
    }

    public static async WaitForClickable(xpath: string)
    {
        var ele: ElementFinder = await element(by.xpath(xpath));

        if (await ele.isEnabled())
        {

            await browser.wait(ExpectedConditions.elementToBeClickable(ele), 5000);
        }

        return ele;
    }


    public static async WaitForClickable_(element: ElementFinder)
    {

        if (await element.isEnabled())
        {

            await browser.wait(ExpectedConditions.elementToBeClickable(element), 5000);
        }

        return element;
    }

    public static async WaitForClickableAndClick(xpath: string)
    {
        var ele: ElementFinder = await element(by.xpath(xpath));

        await browser.wait(ExpectedConditions.elementToBeClickable(ele), 5000);

        await ele.click();

    }


    public static async GetElementAttribute(xpath: string, attribute: string): Promise<string>
    {
        var result = await element(by.xpath(xpath)).getAttribute(attribute)
        return result;
    }

    public static async ScrollToElement(element: ElementFinder)
    {
        await browser.executeScript("arguments[0].scrollIntoView();", element.getWebElement());

    }
}