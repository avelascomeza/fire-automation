import { browser, by } from 'protractor/built';
import { ElementHelper } from 'protractor/built/browser';

import { ShelfType } from '../Enums/enums';
import { ElementController } from './ElementController';


export class SelectController
{
    static GetShelfPath(shelfType: ShelfType): string
    {
        let name;

        if (shelfType == ShelfType.multiselect)
        {
            name = "multi-select";
        }

        return "//eso-" + name + "-shelf";
    }

    static GetPanelfPath(shelfType: ShelfType): string
    {
        let name;

        if (shelfType == ShelfType.multiselect)
        {
            name = "multi-select";
        }

        return "//eso-" + name + "-panel";
    }


    static ClickOkButton(shelfType: ShelfType)
    {
        return ElementController.ClickElement(this.GetShelfPath(shelfType) + "//button[text()='OK']");
    }

    static async SelectItemsInMultiSelect(itemsToSelect: string[])
    {
        var selectedItems = await ElementController.GetAllElements(this.GetPanelfPath(ShelfType.multiselect) + "//li//check-mark[contains(@class,'selected')]");

        if (selectedItems.length > 0)
        {
            for (let selected of selectedItems)
            {
                var itemId = await selected.getAttribute('data-itemid');
                if (itemsToSelect.some(i => i == itemId))
                {
                    itemsToSelect.splice(itemsToSelect.indexOf(itemId), 1);
                }
                else
                {
                    selected.click();
                }
            }

        }

        if (itemsToSelect.length > 0)
        {

            var visibleItems = await ElementController.GetAllElements(this.GetPanelfPath(ShelfType.multiselect) + "//li");


            for (let item of visibleItems)
            {
                var itemId = await item.getAttribute('data-itemid');

                if (itemsToSelect.some(id => id == itemId))
                {
                    itemsToSelect.splice(itemsToSelect.indexOf(itemId), 1);
                   await item.click();
                   
                }
              
            }

        }
    
        await this.ClickOkButton(ShelfType.multiselect);

        await browser.waitForAngular();

    }


}