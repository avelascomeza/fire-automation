

export class DataService
{

  public static async Get(entityUrl:string, property: string, value:string)
  {
    var fetch = require('node-fetch');

    var req = await fetch('http://localhost/Automation/api/'+ entityUrl +'?' + property +'=' + value);

    var result= await req.json();

    return result;
  }
}