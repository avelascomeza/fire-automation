import { browser } from 'protractor/built';
import { defaultThrottleConfig } from 'rxjs/operator/throttle';

import { FieldType } from '../Enums/enums';
import { Tab } from '../pagenavigationcontroller';
import { ElementController } from './ElementController';
import {Field} from './Field';
import { FieldValue } from './fieldValue';
import { ValueController } from './ValueController';
import { ValueSetter } from './valueSetter';

export class TabValueClearer{

    public static async ClearTabValues(tab:Tab)
    {
        var currentTabValues = ValueController.CurrentFieldValues.filter(f => f.Field.TabID == tab);

        if(currentTabValues.length == 0) return;


        //don't worry if they are initial values??
        //i'll do this later

        for(let val of currentTabValues)
        {
           var valCopy = JSON.parse(JSON.stringify(val));
           valCopy.Value = null;
            await ValueSetter.SetField(valCopy);
            
        };



        
    }

    private static async ClearWildlandValues()
    {
        //todo make this generic
        await ValueSetter.SetField(new FieldValue(new Field("AREATYPEID", FieldType.SingleSelect, Tab.wildland), null));
                

    }
    public static async DeleteAllUnitsIgnoreMessage()
    {
        var rows = await ElementController.GetAllElements("//grid-row//grid-cell/button");

        if (rows.length == 0) return;

        await ElementController.ClickElement("//grid-row[1]/div/grid-cell/button");

        await ElementController.ClickElement("//li[text()='Delete']");

        await browser.waitForAngular();

        await ElementController.ClickElement("//button[text()='Delete the unit report']");

        await browser.waitForAngular();

        await this.DeleteAllUnitsIgnoreMessage();
    }


    public static async DeleteAllPatients()
    {
        var rows = await ElementController.GetAllElements("//grid-row//grid-cell/button");

        if (rows.length == 0) return;

        await ElementController.ClickElement("//grid-row[1]//grid-cell/button");

        await ElementController.ClickElement("//li[text()='Delete']");

        await browser.waitForAngular();

        await this.DeleteAllUnitsIgnoreMessage();
    }


    
    private static async DeleteAllInTabItemCollection(itemCollectionName: string)
    {
        var civilians = await ElementController.GetAllElements("//grid-row[grid-cell[tag-text[text()='" + itemCollectionName + "']]]")

        if (civilians.length == 0) return;

        await ElementController.WaitForClickableAndClick("//grid-row[grid-cell[tag-text[text()='" + itemCollectionName + "']]][1]/grid-cell/button");

        await ElementController.ClickElement("//li[text()='Delete']");

        await browser.waitForAngular();

        await this.DeleteAllInTabItemCollection(itemCollectionName);

    }

    public static async DeleteAllCivilianCasualtiesIgnoreError()
    {
        var civilians = await ElementController.GetAllElements("//casualty-tab//grid-row[grid-cell[tag-text[text()='Civilian']]]")

        if (civilians.length == 0) return;

        await ElementController.WaitForClickableAndClick("//casualty-tab//grid-row[grid-cell[tag-text[text()='Civilian']]][1]/grid-cell/button");

        await ElementController.ClickElement("//li[text()='Delete']");

        await browser.waitForAngular();

        await this.DeleteAllCivilianCasualtiesIgnoreError();

    }

    public static async DeleteAllFireCasualtiesIgnoreError()
    {
        var civilians = await ElementController.GetAllElements("//casualty-tab//grid-row[grid-cell[tag-text[text()='Fire Service']]]")

        if (civilians.length == 0) return;

        await ElementController.WaitForClickableAndClick("//casualty-tab//grid-row[grid-cell[tag-text[text()='Fire Service']]][1]/grid-cell/button");

        await ElementController.ClickElement("//li[text()='Delete']");

        await browser.waitForAngular();

        await this.DeleteAllCivilianCasualtiesIgnoreError();

    }

}