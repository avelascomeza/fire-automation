import { escape } from 'querystring';
import { protractor } from 'protractor/built/ptor';
import * as console from 'console';
import { browser, by, element } from 'protractor/built';
import { ElementFinder } from 'protractor/built/element';
import { isNullOrUndefined } from 'util';
import { FieldValue } from './fieldValue';
import { FieldType } from '../Enums/enums';
import { PageNavigationController } from '../pagenavigationcontroller';
import { ElementController } from './ElementController';
import { ListFieldValue } from './ListFieldValue';
import { ValueController } from './ValueController';

let DateFormat = require('dateformat');

export class ValueSetter
{
    public static async SetField(fieldVal: FieldValue, isComplexSubValue: boolean = false, ignoreErrors: boolean = false)
    {
        var currentState = ValueController.CurrentFieldValues.find(v => v.Field.FieldRef == fieldVal.Field.FieldRef);

        if (currentState != null && JSON.stringify(currentState.Value) == JSON.stringify(fieldVal.Value))
        {
            return;
        }
        else if (currentState == null && fieldVal.Value == null)
        {
            //ValueController.CurrentFieldValues.push(fieldVal);
            //return;
        }


        try
        {
            await PageNavigationController.NavigateToTabReturnSuccess(fieldVal.Field.TabID);

            await this.NavigateToField(fieldVal);

            if (fieldVal.Field.DataTypeID != FieldType.CollectionWithData && fieldVal.Field.DataTypeID != FieldType.Boolean)
            {
                await ElementController.WaitForClickableAndClick('//*[@field-ref="' + fieldVal.Field.FieldRef + '"]');
            }
        }
        catch (err)
        {
            if (fieldVal.Value == null)
            {
                var index = ValueController.CurrentFieldValues.indexOf(currentState);

                ValueController.CurrentFieldValues[index].Value = fieldVal.Value;

                return;

            }
            //keep going if its a special snowflake like complex collections
            else //if (fieldVal.Field.DataTypeID != FieldType.CollectionWithData)
            {
                throw err;
            }
        }

        if (fieldVal.Field.DataTypeID == FieldType.String)
        {
            await this.SetStringValue(fieldVal);
        }
        else if (fieldVal.Field.DataTypeID == FieldType.DateTime)
        {
            await this.SetDateValue(fieldVal);
        }
        else if (fieldVal.Field.DataTypeID == FieldType.SingleSelect || fieldVal.Field.DataTypeID == FieldType.NestedSingleSelect)
        {
            await this.SetSingleSelectValue(fieldVal);
        }
        else if (fieldVal.Field.DataTypeID == FieldType.CollectionWithData)
        {
            await this.SetComplexCollection(fieldVal, currentState);
        }
        else if (fieldVal.Field.DataTypeID == FieldType.Boolean)
        {
            await this.SetYesNo(fieldVal);
        }
        else if (fieldVal.Field.DataTypeID == FieldType.Integer)
        {
            await this.SetNumber(fieldVal);
        }
        await browser.waitForAngular();


        if (ignoreErrors && await ElementController.IsPresent("//eso-confirm-modal-dialog"))
        {
            await ElementController.WaitForClickableAndClick("//eso-confirm-modal-dialog//button[text()='Continue']")
            await browser.waitForAngular();
        }

        if (!isComplexSubValue)
        {
            if (currentState == null)
            {
                ValueController.CurrentFieldValues.push(fieldVal);
            }
            else
            {
                var index = ValueController.CurrentFieldValues.indexOf(currentState);

                ValueController.CurrentFieldValues[index].Value = fieldVal.Value;
            }

        }
    }

    public static async NavigateToField(fieldVal: FieldValue)
    {

        if (fieldVal.Field.DataTypeID == FieldType.CollectionWithData && fieldVal.Field.ID != null)
        {

            var lastPeriodInId = fieldVal.Field.ID.lastIndexOf('.');

            var nameAfterLastPeriod = fieldVal.Field.ID.substring(lastPeriodInId + 1);



            var addButtonXpath = "//button[contains(@ng-if,'" + nameAfterLastPeriod + "')]";



            if (await ElementController.IsPresent(addButtonXpath))
            {
                var elem = await ElementController.WaitForClickable(addButtonXpath);


                await browser.executeScript("arguments[0].scrollIntoView();", elem.getWebElement());
                return await browser.waitForAngular();
            }
            else if (await ElementController.IsPresent('//*[@field-ref="' + fieldVal.Field.FieldRef + '"]'))
            {

                var elem = await ElementController.WaitForClickable('//*[@field-ref="' + fieldVal.Field.FieldRef + '"]');

                await browser.executeScript("arguments[0].scrollIntoView();", elem.getWebElement());
                return await browser.waitForAngular();
            }

        }


        if (fieldVal.Field.IsVirtual)
        {
            var elem = await ElementController.GetIfPresent('//*[contains(translate(@ng-model, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"), "' + fieldVal.Field.FieldRef.toLowerCase() + '" )]');

            if (elem == null)
            {
                elem = await ElementController.GetIfPresent('//*[contains(@field-label,"' + fieldVal.Field.DisplayName + '")]');
            }
            await ElementController.WaitForClickable_(elem);
            


            await browser.executeScript("arguments[0].scrollIntoView();", elem.getWebElement());
        }
        else
        {



            var elem = await ElementController.WaitForClickable('//*[@field-ref="' + fieldVal.Field.FieldRef + '"]');

            await browser.executeScript("arguments[0].scrollIntoView();", elem.getWebElement());
        }
        return await browser.waitForAngular();
    }


    private static async SetStringValue(fieldVal: FieldValue)
    {
        var elem: ElementFinder = await element(by.xpath('//*[@field-ref="' + fieldVal.Field.FieldRef + '"]/*[@type="text"]'));

        await elem.clear();

        if (fieldVal.Value != null)
        {

            if ((fieldVal.Value as string).length > 200)
            {
                var parent = await elem.element(by.xpath('..'));
                await browser.executeScript(" var tempInput = document.createElement('textarea'); arguments[0].appendChild(tempInput); tempInput.value = '" + fieldVal.Value + "'; tempInput.setAttribute('id','tempID'); return;",
                    parent.getWebElement());


                var temp = await ElementController.GetElement("//textarea[@id='tempID']");

                await temp.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, "a"));
                await temp.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, "c"));

                await browser.executeScript("arguments[0].remove()", temp);


                await elem.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, "v"));

            }
            else
            {
                await elem.sendKeys(fieldVal.Value);
            }

        }

    }

    private static async SetNumber(fieldVal: FieldValue)
    {
        var elem: ElementFinder = await element(by.xpath('//*[@field-ref="' + fieldVal.Field.FieldRef + '"]/*[@type="number"]'));

        await elem.clear();

        if (fieldVal.Value != null)
        {

            await elem.sendKeys(fieldVal.Value);

        }

    }
    private static async SetDateValue(fieldVal: FieldValue)
    {


        var searchTextField: ElementFinder = await element(by.xpath('//eso-field/eso-control/eso-masked-input/input[@placeholder="mm/dd/yyyy"]'));



        await searchTextField.sendKeys(DateFormat(fieldVal.Value, "mmddyyyy"));



        return await element(by.xpath('//eso-date-panel/shelf-panel/*/header/div[@class="button"]/button')).click().then(() => browser.waitForAngular());




        //input[@type="text"]
    }

    private static async SetYesNo(fieldVal: FieldValue)
    {
        let button: ElementFinder;
        if (fieldVal.Field.IsVirtual)
        {
            button = await ElementController.WaitForClickable('//*[contains(translate(@ng-model, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"), "' + fieldVal.Field.FieldRef.toLowerCase() + '" )]//button');
        }
        else
        {
            if (await ElementController.IsPresent('//*[@field-ref="' + fieldVal.Field.FieldRef + '"]//button[@data-val="' + fieldVal.Value + '"]'))
            {

                button = await element(by.xpath('//*[@field-ref="' + fieldVal.Field.FieldRef + '"]//button[@data-val="' + fieldVal.Value + '"]'));
            }
            else
            {
                button = await element(by.xpath('//*[@field-ref="' + fieldVal.Field.FieldRef + '"]//button[@data-val="' + !fieldVal.Value + '"]'));
            }

        }
        var itemCls = await button.getAttribute('class');

        if ((!itemCls.includes("selected") && fieldVal.Value) || (itemCls.includes("selected") && fieldVal.Value == false))
        {
            await button.click();
        }


    }
    private static async SetSingleSelectValue(fieldVal: FieldValue)
    {
        let val: ListFieldValue = fieldVal.Value;
        await browser.waitForAngular();

        if (fieldVal.Value == null)
        {
            if (await ElementController.IsPresent("//li//div[text()='Clear Selection']"))
            {
                await ElementController.WaitForClickableAndClick("//li//div[text()='Clear Selection']");
            }
            else
            {
                await ElementController.WaitForClickableAndClick("//eso-shelf//button[text()='OK']");
            }
            return;
        }



        var fields = await ElementController.GetAllElements("//li//div[text()='" + val.ItemName + "']");

        if (fields.length == 1)
        {
            await fields[0].click();


            if (await ElementController.IsPresent("//li//div[text()='" + val.ItemName + "']"))
            {
                await ElementController.WaitForClickableAndClick("//eso-shelf//button[text()='OK']");
            }

            return;
        }

        let parent: ElementFinder[] = null;

        for (let plusOne of val.PlusOneCodes)
        {
            parent = await ElementController.GetAllElements("//li[contains(@class,'ng-scope category-item')]//div[starts-with(text(),'(" + plusOne + ")')]");

            if (!isNullOrUndefined(parent) && parent.length > 0)
            {
                break;
            }
        }


        if (!isNullOrUndefined(parent) && parent.length > 0)
        {
            var parEl = parent[0];
            await browser.executeScript("arguments[0].scrollIntoView();", parEl.getWebElement());


            await parEl.click();

            await browser.waitForAngular();
            await this.SetSingleSelectValue(fieldVal);
            return;
        }
        //need to scroll down if we got here
        else 
        {
            var allVisible = await ElementController.GetAllElements("//li//div[contains(@class,'label-container')]");


            var lastChild = allVisible[allVisible.length - 1];


            await ElementController.ScrollToElement(lastChild);

            await this.SetSingleSelectValue(fieldVal);
            return;
        }






    }

    private static async SetComplexCollection(fieldVal: FieldValue, currentValue: FieldValue)
    {

        let addbuttonXpath;
        let okButtonXpath;

        if (fieldVal.Field.ID.startsWith('casualty') && fieldVal.Field.ID.endsWith("Casualties"))
        {
            var lastPeriodInId = fieldVal.Field.ID.lastIndexOf('.');

            var nameAfterLastPeriod = fieldVal.Field.ID.substring(lastPeriodInId + 1);



            addbuttonXpath = "//button[contains(@ng-if,'" + nameAfterLastPeriod + "')]";
            okButtonXpath = "//main-viewport/header//button[text()='OK']";
        }
        else if (fieldVal.Field.ID.startsWith('ems'))
        {
            addbuttonXpath = "//button[contains(@ng-if,'patients')]";
            okButtonXpath = "//main-viewport/header//button[text()='OK']";
        }
        else if (fieldVal.Field.ID.startsWith('units'))
        {
            addbuttonXpath = "//button[contains(@ng-if,'unitReports')]";
            okButtonXpath = "//main-viewport/header//button[text()='OK']";
        }
        else if (fieldVal.Field.ID.startsWith('basic.location.address') || fieldVal.Field.ID.startsWith('arson.agencyReferredTo.address'))
        {

            addbuttonXpath = '//eso-address-summary[contains(@field-label,"' + fieldVal.Field.DisplayName + '")]';
            okButtonXpath = "//shelf-panel//header//button[text()='OK']";
        }
        else if (fieldVal.Field.ID.startsWith('basic.personEntity.owner'))
        {
            var lastPeriodInId = fieldVal.Field.ID.lastIndexOf('.');

            var nameAfterLastPeriod = fieldVal.Field.ID.substring(lastPeriodInId + 1);
            addbuttonXpath = "//button[contains(@ng-if,'" + nameAfterLastPeriod + "')]";
            okButtonXpath = "//shelf-panel//header//button[text()='OK']";
        }
        else
        {
            addbuttonXpath = '//*[@field-ref="' + fieldVal.Field.FieldRef + '"]//grid-cell[contains(@class,"add")]/button';
            okButtonXpath = "//shelf-panel//header//button[text()='OK']";
        }



        if (currentValue == null)
        {

            var arrayOfArrays: [FieldValue[]] = fieldVal.Value;

            for (let fieldValueArray of arrayOfArrays)
            {
                await ElementController.WaitForClickableAndClick(addbuttonXpath);

                if (fieldVal.Field.ID.startsWith("units"))
                {
                    var unit = fieldValueArray.filter(fv => fv.Field.FieldRef == "UNITID")[0];

                    //exception if no unitid
                    if (isNullOrUndefined(unit)) throw "REQUIRES UNIT ID!";


                    await this.SetSingleSelectValue(unit);

                    await browser.waitForAngular();

                    //now click that guy
                    await ElementController.WaitForClickableAndClick("//grid-row//strong[contains(text(),'" + unit.Value.ItemName + "')]");

                }


                for (let val of fieldValueArray)
                {
                    await this.SetField(val, true);
                }

                await ElementController.WaitForClickableAndClick(okButtonXpath);
            }
        }
        else if (fieldVal.Value == null)
        {
            //are there anyRows
            var editButtons = await ElementController.GetAllElements("//*[@field-ref='" + fieldVal.Field.FieldRef + "']//grid-cell[contains(@class,'remove')]/button");

            if (editButtons.length > 0)
            {
                await editButtons[0].click();
                await ElementController.WaitForClickableAndClick("//li[text()='Delete']");
                await browser.waitForAngular();

                if (await ElementController.IsPresent("//eso-modal-dialog"))
                {
                    await ElementController.WaitForClickableAndClick("//eso-modal-dialog//button[text()='OK']");
                    await browser.waitForAngular();
                }
            }

            if (editButtons.length > 1)
            {
                await this.SetComplexCollection(fieldVal, currentValue);
            }


        }
        else
        {

            var tempVal = JSON.parse(JSON.stringify(fieldVal));
            tempVal.Value = null;
            await this.SetComplexCollection(tempVal, currentValue);

            await this.SetComplexCollection(fieldVal, null);
        }

    }
}