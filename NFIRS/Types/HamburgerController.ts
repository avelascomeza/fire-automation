import { by } from 'protractor/built';
import { element, browser } from 'protractor';
import { ElementFinder } from 'protractor/built/element';
import { ElementController } from './ElementController';
import { VeilController } from './VeilController';
export class HamburgerController
{
    private static HamburgerButtonXpath: string = "//header/section/button[contains(@class,'hamburger-bg')]";
    static PrintButton: string = '//div[contains(@class,"popover-menu-container")]/ul/li[text()="Print"]';

    static NextButton: string = "//print-choices-dialog//div[contains(@class,'button-set')]/button[text()='Next']";

    static CancelButton: string = "//print-choices-dialog//div[contains(@class,'button-set')]/button[text()='Cancel']";
    static async OpenHamburger()
    {
        await ElementController.ClickElement(this.HamburgerButtonXpath);

        await browser.waitForAngular();

    }

    public static ClickCancel()
    {
        return ElementController.ClickElement(this.CancelButton);
    }

    static async TryFindElementInPrint(itemsToFind: string[])
    {
        await this.OpenHamburger();
        await ElementController.WaitForClickableAndClick(this.PrintButton);

        await browser.waitForAngular();

        await VeilController.WaitForSpinnerToBeHidden();

        let allFound = true;

        for (let item of itemsToFind)
        {
            var count = await element.all(by.xpath("//print-choices-dialog//div[div[div[text()='" + item + "']]]")).count();

            if (count == 0)
            {
                allFound = false;
                break;
            }
        }


        let checkBoxes: ElementFinder[] = await ElementController.GetAllElements("//print-choices-dialog//check-mark");


        await this.ClickCancel();

        await browser.waitForAngular();

        return allFound;

    }

    static async Print(itemsToClick: string[])
    {
        await this.OpenHamburger();
        await ElementController.WaitForClickableAndClick(this.PrintButton);
        await browser.waitForAngular();

        await VeilController.WaitForSpinnerToBeHidden();
        let checkBoxes: ElementFinder[] = await ElementController.GetAllElements("//print-choices-dialog//check-mark");



        for (let item of checkBoxes)
        {

            if (await item.isDisplayed())
            {
                var clicked = (await item.getAttribute("class")).includes("selected");

                if (clicked)
                {
                    await item.click();
                }
            }
        }



        try
        {
            for (let item of itemsToClick)
            {
                var parent: ElementFinder = await element(by.xpath("//print-choices-dialog//div[div[div[text()='" + item + "']]]"));

                var checkbox: ElementFinder = await parent.element(by.xpath(".//check-mark"));

                var clicked = (await checkbox.getAttribute("class")).includes("selected");

                if (!clicked)
                {
                    await checkbox.click();
                }
            }
            
            await element(by.xpath(this.NextButton)).click();
        }
        catch(err)
        {
            await this.ClickCancel();

            await browser.waitForAngular();
            throw err;
        }



        await VeilController.WaitForSpinnerToBeHidden();
    }
    static async OpenAndCancelPrint()
    {
        await this.OpenHamburger();
        await element(by.xpath(this.PrintButton)).click();

        await browser.waitForAngular();

        await this.ClickCancel();

        await browser.waitForAngular();

    }

    static async PrintNothing()
    {
        await this.OpenHamburger();
        await element(by.xpath(this.PrintButton)).click();

        await browser.waitForAngular();

        var checkBoxes: ElementFinder[] = await element.all(by.xpath("//print-choices-dialog//check-mark"));

        for (let item of checkBoxes)
        {

            if (await item.isDisplayed())
            {
                var clicked = (await item.getAttribute("class")).includes("selected");

                if (clicked)
                {
                    await item.click();
                }
            }
        }

        await element(by.xpath(this.NextButton)).click();

        await browser.waitForAngular();
    }

    public static async ClosePrintView()
    {
        await ElementController.ClickElement("//print-view//button");

        await browser.waitForAngular();
    }

}

