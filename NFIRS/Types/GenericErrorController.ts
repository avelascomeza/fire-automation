
import { element, browser } from 'protractor';
import { ElementController } from './ElementController';
import { ElementFinder } from 'protractor/built/element';
export class GenericErrorController
{
    static ErrorPath: string = "//generic-error";
    static Message: string = "//generic-error//div[@class='content']//span[@ng-if='message']";
    static Title: string = "//generic-error/div/header"

    static OkButton: string = "//generic-error//button[text()='OK']";


    public static IsErrorVisible(): Promise<boolean>
    {
        return ElementController.IsPresent(this.ErrorPath);

    }

    public static async GetTitleAndMessage(): Promise<[string, string]>
    {   
        return [await  ElementController.GetTextElementText(this.Title), await ElementController.GetTextElementText(this.Message)];
    }

    public static ClickOkButton(): Promise<void>
    {
        return ElementController.ClickElement(this.OkButton).then(() => browser.waitForAngular());
    }

}