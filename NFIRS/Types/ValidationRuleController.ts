import { element, by } from 'protractor';
import { Tab } from '../pagenavigationcontroller';
import { ElementFinder } from 'protractor/built/element';
import { browser } from 'protractor/built';
export class ValidationRuleController
{

    private static ValidationIssuesButtonXpath: string = "//header/section/validation";

    public static async GetCurrentValidationRuleCount()
    {
        var count = await element(by.xpath(this.ValidationIssuesButtonXpath + '/div/div')).getAttribute('data-count');

        return count;
    }

    public static async OpenValidationRulesWindow()
    {
        return await element(by.xpath(this.ValidationIssuesButtonXpath)).click().then(() => browser.waitForAngular());
    }

    public static async CloseValidatonRuleWindow()
    {
        return await element(by.xpath("//shelf-panel[@heading=\"'Validation Errors'\"]/*/header/div[@class='button']/button")).click().then(() => browser.waitForAngular());
    }


    public static async GetAllValidationRuleElements(): Promise<ValidationRuleToastSection[]>
    {
        await this.OpenValidationRulesWindow();

        var sections:ValidationRuleToastSection[] = [];

        var foundElements:ElementFinder[] = await element.all(by.xpath("//validation-toast/*/*/*/*/*/*/div[@class='validation-details']/section"));

        for(var i = 0; i< foundElements.length; i ++)
        {
            var fnd  = foundElements[i];
            var title = await fnd.element(by.xpath("./header/h2")).getText();

            var childElements:ElementFinder[] = await fnd.all(by.xpath("./div/grid-row"));

            var links:ValidationRuleToastLink[] = [];

            var tabEdit =  title.replace(" Tab","").toLowerCase();
            var nameAsTab:Tab = <Tab>Tab[tabEdit];

            for(var k = 0; k < childElements.length; k++)
            {
                var chld = childElements[k];

                var name = chld.element(by.xpath("./grid-cell/strong")).getText();

                var desciption = chld.element(by.xpath("./grid-cell/aside")).getText();
                links.push(new ValidationRuleToastLink(nameAsTab, await name, await desciption));
            };

            sections.push(new ValidationRuleToastSection(nameAsTab, links));
        };

        return sections;
    }

};
export class ValidationRuleToastSection
{
    Tab:Tab;
    Children:ValidationRuleToastLink[];

    constructor(tab:Tab,children:ValidationRuleToastLink[])
    {
        this.Tab = tab;
        this.Children = children;



    }

};

export class ValidationRuleToastLink
{
    Tab:Tab;
    Name:String;
    Description:string;

    constructor(tab:Tab, name:string, description:string)
    {
        this.Tab = tab;
        this.Name = name;
        this.Description = description;
    }

};



