import { browser } from 'protractor/built';
import { ElementFinder } from 'protractor/built/element';

import { FieldType } from '../Enums/enums';
import { PageNavigationController, Tab } from '../pagenavigationcontroller';
import { DataService } from './DataService';
import { ElementController } from './ElementController';
import { FieldValue } from './fieldValue';
import { ListField, ListFieldValue } from './ListFieldValue';
import { SelectController } from './SelectController';
import { ValueSetter } from './valueSetter';

import { Component } from '@angular/core';
import { Field } from "./Field";

export class SpecialTabHelper
{
    static AddUnitsButton: string = "//div/div/button[text()='Add Units']";

    public static async AddUnit(unitsToAdd: string[])
    {

        await ElementController.WaitForClickableAndClick(this.AddUnitsButton);

        await SelectController.SelectItemsInMultiSelect(unitsToAdd);

    }


    public static async EnableCasualties()
    {
         await this.EnableFire();

        let val: ListFieldValue = (await DataService.Get('ListFieldValues','ItemName', 'Civilian and Fire Service Injuries and/or Deaths'))[0];
        let fieldVal: FieldValue = new FieldValue(new Field("INJURYORDEATHID", FieldType.SingleSelect, Tab.basic), val);


        await ValueSetter.SetField(fieldVal);

    }


    public static async EnableEMS()
    {
        let val: ListFieldValue = {
            ItemID: "359",
            ItemName: "(322) - Motor vehicle accident with injuries",
            ItemNameID: "322",
            PlusOneCodes: ["3", "32"],
            Parent_ItemID: "356",
            ID: "536723c8-b142-4a13-8cbf-b8dbc479a8fc",
            ListField: {
                Name: "NFIRS_INCIDENT_TYPE",
                ID: "NFIRS_INCIDENT_TYPE"
            }
        };




        let fieldVal: FieldValue = new FieldValue( new Field("INCIDENTTYPEID", FieldType.NestedSingleSelect, Tab.basic),val);

        await ValueSetter.SetField(fieldVal);

    }

    public static async EnableHazmat()
    {

        let val: ListFieldValue = (await DataService.Get('ListFieldValues','ItemName', '(451) - Biological hazard, confirmed or suspected'))[0];

        let fieldVal: FieldValue = new FieldValue(new Field("INCIDENTTYPEID",FieldType.NestedSingleSelect, Tab.basic), val);

        let hazamatVal: ListFieldValue = (await DataService.Get('ListFieldValues','ItemName', '(0) - Special HazMat actions required or spill >= 55 gal.'))[0];

        let hazmAtFV: FieldValue = new FieldValue(new Field("HAZMATRELEASE",  FieldType.SingleSelect, Tab.basic),hazamatVal);
        await ValueSetter.SetField(fieldVal);

        await browser.waitForAngular();
        await ValueSetter.SetField(hazmAtFV);


        await browser.waitForAngular();
    }

    public static async EnableWildland()
    {

        let val: ListFieldValue = (await DataService.Get('ListFieldValues','ItemName', '(141) - Forest, woods or wildland fire'))[0];

        let fieldVal: FieldValue = new FieldValue(new Field("INCIDENTTYPEID", FieldType.NestedSingleSelect, Tab.basic), val);

        let wildlandOrFire: ListFieldValue = (await DataService.Get('ListFieldValues','ItemName', 'Wildland Module'))[0];

        let wildandFV: FieldValue = new FieldValue(new Field("FIREORWILDLAND", FieldType.SingleSelect, Tab.basic), wildlandOrFire);
        await ValueSetter.SetField(fieldVal);

        await browser.waitForAngular();
        await ValueSetter.SetField(wildandFV);


        await browser.waitForAngular();
    }


    public static async EnableArson()
    {
        await this.EnableFire();

        let val: ListFieldValue = (await DataService.Get('ListFieldValues','ItemName', '(1) - Intentional'))[0];

        var fieldVal = new FieldValue(new Field("CAUSEOFIGNITIONID", FieldType.NestedSingleSelect, Tab.fire),val);

        await ValueSetter.SetField(fieldVal);


    }


    public static async EnableFire()
    {

        let val: ListFieldValue = (await DataService.Get('ListFieldValues','ItemName', '(100) - Fire, other'))[0];

        let fieldVal: FieldValue = new FieldValue(new Field("INCIDENTTYPEID",FieldType.NestedSingleSelect, Tab.basic), val);


        await ValueSetter.SetField(fieldVal);


    }

    public static async EnableStructureFire()
    {

        let val: ListFieldValue = (await DataService.Get('ListFieldValues','ItemName', '(113) - Cooking fire, confined to container'))[0];

        let fieldVal: FieldValue = new FieldValue(new Field("INCIDENTTYPEID", FieldType.NestedSingleSelect, Tab.basic), val);


        await ValueSetter.SetField(fieldVal);

        await browser.waitForAngular();


    }

    public static async AddItemToTabItemCollection(addText: string)
    {
        await ElementController.WaitForClickableAndClick("//div/button[text()='" + addText + "']");

        await browser.waitForAngular();

        await ElementController.WaitForClickableAndClick("//main-viewport//button[text()='OK']");

        await browser.waitForAngular();

    }


    public static async AddOwner(involved: boolean)
    {
        await PageNavigationController.NavigateToTabReturnSuccess(Tab.basic);

        await ValueSetter.NavigateToField(new FieldValue(new Field('PERSONCOLLECTION',FieldType.CollectionWithData, Tab.basic), null));

        if (await ElementController.IsPresent("//custom-complex-collection//button[text()='Add Owner']"))
        {
            await ElementController.ClickElement("//custom-complex-collection//button[text()='Add Owner']");


            var setInvolved = new FieldValue(new Field('OWNERINVOLVEDININCIDENT',FieldType.Boolean, Tab.basic),involved);
            var firstName =  new FieldValue(new Field('OWNERFIRSTNAME', FieldType.String, Tab.basic),"Herbert");

            await ValueSetter.SetField(setInvolved, true);
            await ValueSetter.SetField(firstName, true);

            await ElementController.WaitForClickableAndClick("//shelf-panel//header//button[text()='OK']");

            await browser.waitForAngular();
        }
        else
        {
            throw "NO";
        }


    }

    public static async EditOwner(isInvolved:boolean)
    {
        await PageNavigationController.NavigateToTabReturnSuccess(Tab.basic);

        await ValueSetter.NavigateToField(new FieldValue(new Field('PERSONCOLLECTION', FieldType.CollectionWithData, Tab.basic), null));


        if (await ElementController.IsPresent("//custom-complex-collection//grid-row[grid-cell[span[text()='OWNER']]]//button"))
        {

            await ElementController.ClickElement("//custom-complex-collection//span[text()='OWNER']");
            
            await browser.waitForAngular();
            
            var setInvolved = new FieldValue(new Field('OWNERINVOLVEDININCIDENT',FieldType.Boolean, Tab.basic),isInvolved);
          

            await ValueSetter.SetField(setInvolved, true);

            
            await ElementController.WaitForClickableAndClick("//shelf-panel//header//button[text()='OK']");
        }
        await browser.waitForAngular();
    }
    

}