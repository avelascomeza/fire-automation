export interface ListField
{
    Name: string;
    ID: string;
}
export interface ListFieldValue
{
    ID: string;
    ItemID: string;
    ItemName:string;
    ItemNameID:string;
    ListField:ListField;

    Parent_ItemID:string;
    PlusOneCodes:string[];   
}