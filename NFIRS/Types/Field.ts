import { FieldType } from '../Enums/enums';
import { Tab } from '../pagenavigationcontroller';

export class Field
{
    public ID: string;
    public FieldRef: string;
    public FieldLabel: string;

    public DataTypeID: FieldType;

    public TabID: Tab;

    public IsRequired: boolean = false;
    public IsEnabled: boolean = false;
    public DisplayName: string;
    public IsVirtual: boolean = false;


    constructor(fieldref: string, fieldType: FieldType, tab: Tab)
    {
        this.FieldRef = fieldref;
        this.DataTypeID = fieldType;
        this.TabID = tab;
        this.IsEnabled;


    };
};