import { FieldValue } from './fieldValue';


export class TestExecutionRule
{
    public Jira:string;
    public RuleDescription: string;
    public Type: string;

    public RequiresRefresh:boolean;
    public Name: string;
    public Setup:[FieldValue];

   public Act:[FieldValue];

   public Assert:[FieldValue];

}