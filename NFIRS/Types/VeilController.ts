import { ElementController } from './ElementController';
import { browser, ExpectedConditions } from 'protractor';
import { ElementFinder } from 'protractor/built/element';
import * as console from 'console';
export class VeilController
{
    static VeilSpinner: string = "//eso-modal//div[contains(@class,'loading-spinner')]";


    public static async WaitForSpinnerToBeHidden()
    {
        try
        {
            var ele: ElementFinder = await ElementController.GetElement(this.VeilSpinner)

            return await browser.wait(ExpectedConditions.not(ExpectedConditions.presenceOf(ele)));
        }
        catch (error)
        {
            throw error;
        }

    }
}