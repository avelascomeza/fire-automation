import { $, protractor, browser, element, by, ExpectedConditions, } from 'protractor';
import { GlobalConfigs } from './protractorConfig';
import { resolve } from 'path';
import { ValueController } from './Types/ValueController';




// The Expected Conditions library from Protractor, which is useful for checking
// the existence and visibility of elements on the DOM.
let toSee = ExpectedConditions;

export class NFIRSInitializer
{
    static onPrepare()
    {

        //nuke the folder if it exists

        //     var del = require('del');
        //    del.sync(["./reports/" + `${(new Date() as any).format("{FullYear}{Month:2}{Date:2}")}` + "/**"]);

        // Add a reporter that outputs to log. 
        const SpecReporter = require("jasmine-spec-reporter").SpecReporter;
        jasmine.getEnv().addReporter(new SpecReporter({
            spec: { displayStacktrace: GlobalConfigs.verbose },
            summary: {
                displayDuration: true,
                displayPending: true,
            },
            gatherBrowserLogs: true,
            preserveDirectory: false,
            displayPending: true
        }));

        var env = jasmine.getEnv();
  


        // Add a screenshot reporter and store screenshots to `/tmp/screenshots`: 
        const HtmlReporter = require("protractor-beautiful-reporter");
        jasmine.getEnv().addReporter(new HtmlReporter({
            // baseDirectory: "./reports/" + `${(new Date() as any).format("{FullYear}{Month:2}{Date:2}-{Hours:2}{Minutes:2}{Seconds:2}")}`,
            baseDirectory: "./reportsa",
            screenshotsSubfolder: "images",
            jsonsSubfolder: "json",
            docName: "report.html",
            docTitle: "NFIRS V5 Regression Tests",
            takeScreenShotsOnlyForFailedSpecs: true,
            gatherBrowserLogs: true,
            preserveDirectory: false,
            displayPending: true,
            includeTestdata: true
            //includeInfoLogs: true
        }).getJasmine2Reporter());



        protractor.browser.manage().timeouts().implicitlyWait(500);

    }


    static CreateNewIncident()
    {
        // launching
        browser.ignoreSynchronization = true;
        browser.driver.get(GlobalConfigs.baseUrl);
        browser.waitForAngular();
        browser.ignoreSynchronization = false;
        return element(by.id("AddRecord")).click();

    }

    static loadPage(pageUrl: string, pageDirective: string)
    {
        // Deactivate synch with Angular for manual boostrap. 
        browser.ignoreSynchronization = true;

        // Navigate to the current page.
        browser.driver.get(GlobalConfigs.baseUrl + pageUrl);

        // wait for deferred bootstrap body to not be present
        return browser.wait(toSee.presenceOf($(pageDirective)), 3000)
            .then(() =>
            {
                browser.waitForAngular();
                browser.ignoreSynchronization = false;
            });
    }

    static unloadPage()
    {
        // Remove local and session storage to begin with clean slate.
        browser.executeScript("window.sessionStorage.clear();");
        browser.executeScript("window.localStorage.clear();");
    }

};
