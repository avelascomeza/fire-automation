import { count } from 'rxjs/operator/count';

import { Config } from 'protractor';
import { NFIRSInitializer } from './nfirsinitializer';

export class GlobalConfigs
{
    static verbose: boolean = false;
    static baseUrl: string = "http://localhost/fire/nfirs/";
    static defaultPage: string = "/#/search?template=1"
}

export let config: Config = {
    framework: "jasmine",
    // verbose: false,
    directConnect: true,
    moduleName: "NFIRS",
    specs: [
        'specs/BusinessRules_spect.js'],
    //'specs/print_spec.js'],

    onPrepare: function ()
    {

        // Load our helper libraries, these add some custom extension methods.
        require("./helpers/wait-absent.js");
        require("./helpers/wait-ready.js");
        require("./helpers/date-helpers.js");



        // Run shared initialization steps, such as adding new prototypal (extension) methods,
        // setting some protractor properties, and adding reporters to Jasmine. 

        return NFIRSInitializer.onPrepare();
        //NFIRSInitializer.CreateRecordAndLoadData();


        // Log in to the ESO Suite. Return makes it async, preventing us from 
        // starting tests until the promise resolves.

    },

    // Browser capabilities.
    capabilities: {
        name: "Win10/Chrome",
        platform: "Windows 10",
        browserName: "chrome",
        'chromeOptions': { args: ["--test-type", "--ignore-certificate-errors", "--start-maximized"] },
       // loggingPrefs: { "driver": "INFO", "browser": "INFO" }
    },

    // Options to be passed to Jasmine-node.
    jasmineNodeOpts: {
        //print: function () { /* Prevent the default spec dots from appearing in the output */ }
    },



}



// import {Config} from 'protractor';

// export let config: Config= {
//     framework: "jasmine",
//     seleniumAddress: "http://localhost:4444/wd/hub",
//     specs: NFIRSConfig.testSpecs,
//    

