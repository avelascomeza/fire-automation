

export enum FieldType
{
     Unknown,
        DateTime,
        SingleSelect,
        String,
        MultiSelect,
        NestedSingleSelect,
        CollectionWithData,
        Boolean,
        Time,
        Binary,
        Integer,
        Virtual,
        Number,
}

export enum ShelfType
{
    multiselect,
    singleselect,
}