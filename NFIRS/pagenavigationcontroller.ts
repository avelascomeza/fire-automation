import { VeilController } from './Types/VeilController';


import { element, by, browser } from 'protractor';
import { ElementFinder } from 'protractor/built/element';

export enum Tab
{
    unknown,
    basic,
    unitReports,
    fire,
    arson,
    civilianCasualty,
    fireCasualty,
    ems,
    wildland,
    hazmat,
};

export class PageNavigationController
{
    static CurrentPage: Tab = Tab.basic;
    static TabName(tab: Tab): string
    {
        for (var k in Tab) if (Tab[k] == tab.toString()) return k;
        return null;
    }


    static async NavigateToTabReturnSuccess(tab: Tab): Promise<boolean>
    {
        if (this.CurrentPage == tab) return true;

        if(tab == Tab.unknown) return false; 
        else if (await this.IsTabEnabled(tab))
        {   
            let tabName =this.TabName(tab);
            if(tab ==Tab.civilianCasualty || tab == Tab.fireCasualty)
            {
                tabName = "casualty";
            }
            var thiSOne: ElementFinder = await element(by.xpath('//nav[@class="app-tabs"]/ul/li[@data-state-id="incident.' + tabName + '"]'));

            await thiSOne.click();

            await browser.waitForAngular();

            await VeilController.WaitForSpinnerToBeHidden();
            this.CurrentPage = tab;
            return true;

        }

        return false;

    }

    static async IsTabEnabled(tab: Tab): Promise<boolean>
    {


        var thiSOne: ElementFinder = await element(by.xpath('//nav[@class="app-tabs"]/ul/li[@data-state-id="incident.' + this.TabName(tab) + '"]'));


        if (thiSOne == undefined) return false;


        try
        {
            var disabled = await thiSOne.getAttribute('disabled');

            return disabled == null;
        }
        catch (exc)
        {
            return true;
        }


    }


};
